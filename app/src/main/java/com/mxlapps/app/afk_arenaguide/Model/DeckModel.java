package com.mxlapps.app.afk_arenaguide.Model;

import java.util.ArrayList;

public class DeckModel {
    private String user_id;
    private String hero1;
    private String hero2;
    private String hero3;
    private String hero4;
    private String hero5;
    private String name;
    private String desc;
    private String section;
    private String game_level;
    private String author;
    private String votes;
    private Integer numero_votos;
    private Integer status;
    private Integer id;
    private ArrayList<CommentsModel> comments;
    private String strongvs;
    private String weakvs;
    private String fb_image;
    private Integer game_level_early;
    private Integer game_level_mid;
    private Integer game_level_late;
    private Integer section_overall;
    private Integer section_pve;
    private Integer section_pvp;
    private Integer section_lab;
    private Integer section_wrizz;
    private Integer section_soren;

    public Integer getNumero_votos() {
        return numero_votos;
    }

    public void setNumero_votos(Integer numero_votos) {
        this.numero_votos = numero_votos;
    }

    public String getFb_image() {
        return fb_image;
    }

    public void setFb_image(String fb_image) {
        this.fb_image = fb_image;
    }

    public String getStrongvs() {
        return strongvs;
    }

    public void setStrongvs(String strongvs) {
        this.strongvs = strongvs;
    }

    public String getWeakvs() {
        return weakvs;
    }

    public void setWeakvs(String weakvs) {
        this.weakvs = weakvs;
    }

    public Integer getGame_level_early() {
        return game_level_early;
    }

    public void setGame_level_early(Integer game_level_early) {
        this.game_level_early = game_level_early;
    }

    public Integer getGame_level_mid() {
        return game_level_mid;
    }

    public void setGame_level_mid(Integer game_level_mid) {
        this.game_level_mid = game_level_mid;
    }

    public Integer getGame_level_late() {
        return game_level_late;
    }

    public void setGame_level_late(Integer game_level_late) {
        this.game_level_late = game_level_late;
    }

    public Integer getSection_overall() {
        return section_overall;
    }

    public void setSection_overall(Integer section_overall) {
        this.section_overall = section_overall;
    }

    public Integer getSection_pve() {
        return section_pve;
    }

    public void setSection_pve(Integer section_pve) {
        this.section_pve = section_pve;
    }

    public Integer getSection_pvp() {
        return section_pvp;
    }

    public void setSection_pvp(Integer section_pvp) {
        this.section_pvp = section_pvp;
    }

    public Integer getSection_lab() {
        return section_lab;
    }

    public void setSection_lab(Integer section_lab) {
        this.section_lab = section_lab;
    }

    public Integer getSection_wrizz() {
        return section_wrizz;
    }

    public void setSection_wrizz(Integer section_wrizz) {
        this.section_wrizz = section_wrizz;
    }

    public Integer getSection_soren() {
        return section_soren;
    }

    public void setSection_soren(Integer section_soren) {
        this.section_soren = section_soren;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getHero1() {
        return hero1;
    }

    public void setHero1(String hero1) {
        this.hero1 = hero1;
    }

    public String getHero2() {
        return hero2;
    }

    public void setHero2(String hero2) {
        this.hero2 = hero2;
    }

    public String getHero3() {
        return hero3;
    }

    public void setHero3(String hero3) {
        this.hero3 = hero3;
    }

    public String getHero4() {
        return hero4;
    }

    public void setHero4(String hero4) {
        this.hero4 = hero4;
    }

    public String getHero5() {
        return hero5;
    }

    public void setHero5(String hero5) {
        this.hero5 = hero5;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getGame_level() {
        return game_level;
    }

    public void setGame_level(String game_level) {
        this.game_level = game_level;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<CommentsModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentsModel> comments) {
        this.comments = comments;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
