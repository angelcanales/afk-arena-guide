package com.mxlapps.app.afk_arenaguide.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.StrengthWeaknessModel;
import com.mxlapps.app.afk_arenaguide.R;

import java.util.ArrayList;

public class ProsConsAdapter extends RecyclerView.Adapter<ProsConsAdapter.SectionViewHolder>{

    private ArrayList<StrengthWeaknessModel> strengthWeaknessModel;
    private OnItemClickListener mlistener;
    Integer tipo;
    Context ctx;


    public void SetOnItemClickListener (OnItemClickListener mlistener) {
        this.mlistener = mlistener;
    }

    public interface OnItemClickListener {
        void onSectionCardClick(int position);
    }


    public ProsConsAdapter(ArrayList<StrengthWeaknessModel> strengthWeaknessModel, Integer tipo, Context ctx) {
        this.strengthWeaknessModel = strengthWeaknessModel;
        this.tipo = tipo;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ProsConsAdapter.SectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_strength_weakness, parent, false);
        return new SectionViewHolder(view, mlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProsConsAdapter.SectionViewHolder holder, int position) {

        holder.textView_s.setText(strengthWeaknessModel.get(position).getDesc());
        if (tipo == 1){
            holder.imageView_proscons_type_icon.setImageDrawable(ctx.getDrawable(R.drawable.positive));
        }else{
            holder.imageView_proscons_type_icon.setImageDrawable(ctx.getDrawable(R.drawable.negative));
        }


    }


    @Override
    public int getItemCount() {
        return strengthWeaknessModel.size();
    }

    public class SectionViewHolder extends RecyclerView.ViewHolder {

        TextView textView_s;
        ImageView imageView_proscons_type_icon;
        CardView cardView_section_item;

        public SectionViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            textView_s   = itemView.findViewById(R.id.textView_proscons_desc);
            imageView_proscons_type_icon    = itemView.findViewById(R.id.imageView_proscons_type_icon);
//            cardView_section_item = itemView.findViewById(R.id.cardView_section_item);
//
//            cardView_section_item.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (listener != null){
//                        listener.onSectionCardClick(getAdapterPosition());
//                    }
//                }
//            });



        }
    }
}
