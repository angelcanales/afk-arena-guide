package com.mxlapps.app.afk_arenaguide.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HeroesDeckAdapter extends RecyclerView.Adapter<HeroesDeckAdapter.HeroViewHolder> {

    private ArrayList<HeroModel> heroModels;
    private OnItemClickListener mlistener;
    Context ctx;

    public void SetOnItemClickListener (OnItemClickListener mlistener) {
        this.mlistener = mlistener;
    }

    public interface OnItemClickListener {
        void onHeroCardClick(int position);
    }

    public HeroesDeckAdapter(ArrayList<HeroModel> heroModels, Context context) {
        this.heroModels = heroModels;
        this.ctx = context;
    }

    @NonNull
    @Override
    public HeroesDeckAdapter.HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hero_deck, parent, false);
        return new HeroViewHolder(view, mlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull final HeroesDeckAdapter.HeroViewHolder holder, int position) {

        HeroModel hero = heroModels.get(position);
        Picasso.get().load(hero.getSmallImage()).into(holder.smallImage);
    }


    @Override
    public int getItemCount() {
        return heroModels.size();
    }

    public class HeroViewHolder extends RecyclerView.ViewHolder {

        ImageView smallImage;
        CardView cardView_pin_item;

        public HeroViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            smallImage = itemView.findViewById(R.id.imageView_hero_image);
            cardView_pin_item = itemView.findViewById(R.id.cardView_section_item);

            cardView_pin_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        listener.onHeroCardClick(getAdapterPosition());
                    }
                }
            });
        }
    }

}
