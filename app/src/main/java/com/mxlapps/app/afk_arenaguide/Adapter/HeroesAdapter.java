package com.mxlapps.app.afk_arenaguide.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

public class HeroesAdapter extends RecyclerView.Adapter<HeroesAdapter.HeroViewHolder> {

    private ArrayList<HeroModel> heroModels;
    private ArrayList<HeroModel> heroModelsFull;
    private OnItemClickListener mlistener;
    Context ctx;
    int modo = 1;


    public void SetOnItemClickListener (OnItemClickListener mlistener) {
        this.mlistener = mlistener;
    }


    public interface OnItemClickListener {
        void onHeroCardClick(int position);
    }



    public HeroesAdapter(ArrayList<HeroModel> heroModels, Context context, int modo) {
        this.heroModels = heroModels;
        this.ctx = context;
        this.modo = modo;
        heroModelsFull = new ArrayList<>(heroModels);
    }

    @NonNull
    @Override
    public HeroesAdapter.HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hero, parent, false);
        return new HeroViewHolder(view, mlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull final HeroesAdapter.HeroViewHolder holder, int position) {
        HeroModel hero = heroModels.get(position);
        Picasso.get().load(hero.getSmallImage()).into(holder.smallImage);
        tierColor(hero.getSection(), holder);
    }


    @Override
    public int getItemCount() {
        return heroModels.size();
    }

    public class HeroViewHolder extends RecyclerView.ViewHolder {
        TextView textView_tier;
        ImageView smallImage;
        ImageView imageview_bg_new;
        CardView cardView_hero_item;

        public HeroViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            imageview_bg_new = itemView.findViewById(R.id.imageview_bg_new);
            smallImage = itemView.findViewById(R.id.imageView_hero_image);
            textView_tier = itemView.findViewById(R.id.textView_tier);
            cardView_hero_item = itemView.findViewById(R.id.cardView_section_item);
            cardView_hero_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        listener.onHeroCardClick(getAdapterPosition());
                    }
                }
            });
        }
    }


    public void tierColor(String tier, HeroesAdapter.HeroViewHolder holder){
        switch (tier){
            case "S+":
                holder.textView_tier.setText("S+");
                holder.textView_tier.setBackgroundColor(Color.parseColor("#a64d79"));
                holder.imageview_bg_new.setImageDrawable(ctx.getDrawable(R.drawable.item_bg_splus));
                break;
            case "S":
                holder.textView_tier.setText("S");
                holder.textView_tier.setBackgroundColor(Color.parseColor("#674ea7"));
                holder.imageview_bg_new.setImageDrawable(ctx.getDrawable(R.drawable.item_bg_s));
                break;
            case "A":
                holder.textView_tier.setText("A");
                holder.textView_tier.setBackgroundColor(Color.parseColor("#3c78d8"));
                holder.imageview_bg_new.setImageDrawable(ctx.getDrawable(R.drawable.item_bg_a));
                break;
            case "B":
                holder.textView_tier.setText("B");
                holder.textView_tier.setBackgroundColor(Color.parseColor("#6aa84f"));
                holder.imageview_bg_new.setImageDrawable(ctx.getDrawable(R.drawable.item_bg_b));
                break;
            case "C":
                holder.textView_tier.setText("C");
                holder.textView_tier.setBackgroundColor(Color.parseColor("#f1c232"));
                holder.imageview_bg_new.setImageDrawable(ctx.getDrawable(R.drawable.item_bg_c));
                break;
            case "D":
                holder.textView_tier.setText("D");
                holder.textView_tier.setBackgroundColor(Color.parseColor("#b45f06"));
                holder.imageview_bg_new.setImageDrawable(ctx.getDrawable(R.drawable.item_bg_d));
                break;
            case "E":
                holder.textView_tier.setText("E");
                holder.textView_tier.setBackgroundColor(Color.parseColor("#b45f06"));
                holder.imageview_bg_new.setImageDrawable(ctx.getDrawable(R.drawable.item_bg_e));
                break;
            case "F":
                holder.textView_tier.setText("F");
                holder.textView_tier.setBackgroundColor(Color.parseColor("#ffffff"));
                holder.imageview_bg_new.setImageDrawable(ctx.getDrawable(R.drawable.item_bg_f));
                break;
        }
    }


}
