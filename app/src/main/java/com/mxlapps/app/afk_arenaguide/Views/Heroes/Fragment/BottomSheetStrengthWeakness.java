package com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.Model.SuggestionModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.Data;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.ExtraApi;
import com.mxlapps.app.afk_arenaguide.Utils.AppPreferences;
import com.mxlapps.app.afk_arenaguide.ViewModel.ExtraViewModel;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressLint("ValidFragment")
public class BottomSheetStrengthWeakness extends BottomSheetDialogFragment {

    private static HeroModel hero;
    AppCompatSpinner spinner;
    TextInputEditText edittext_seugestion;
    ExtraViewModel extraViewModel;
    private View rootView;
    private int hero_id;
    View v;

    public BottomSheetStrengthWeakness(HeroModel hero) {
        this.hero = hero;
        this.hero_id = hero.getId();
    }

    public BottomSheetStrengthWeakness() {

    }

    public static BottomSheetStrengthWeakness getInstance() {
        return new BottomSheetStrengthWeakness();
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.bottom_sheet_strength_weak, container, false);

        eventos();
        llenaSpinner();

        return v;
    }




    private void eventos() {
        edittext_seugestion = v.findViewById(R.id.edittext_seugestion);
        final Button button_procon_save = v.findViewById(R.id.button_procon_save);
        button_procon_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Bloquea el boton
                button_procon_save.setEnabled(false);
                String type = spinner.getSelectedItem().toString();
                String suggestion = (edittext_seugestion.getText() != null) ? edittext_seugestion.getText().toString(): "";

                // Valida que no este vacia la sugerencia
                if (type.compareToIgnoreCase("--") == 0){
                    Toast.makeText(getActivity(), "Select type of Suggestion", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (suggestion.compareToIgnoreCase("") == 0){
                    Toast.makeText(getActivity(), "Suggestion can't be empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Crea objeto para enviarlo al server
                SuggestionModel suggestionModel = new SuggestionModel();
                suggestionModel.setType(type);
                suggestionModel.setSuggestion(suggestion);
                suggestionModel.setUser_token(AppPreferences.getInstance(getActivity()).getUserToken());
                suggestionModel.setHero_id(hero_id);

                Data data = new Data();
                data.setSuggestion(suggestionModel);
                DataMaster dataMaster = new DataMaster();
                dataMaster.setData(data);

                // Se hace request
                String URL_SERVER_DOCDIGITALES = BuildConfig.API_BASE_URL;
                Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
                HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
                httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .addInterceptor(httpLoggingInterceptor)
                        .build();
                Retrofit retrofit = new retrofit2.Retrofit.Builder()
                        .baseUrl(URL_SERVER_DOCDIGITALES)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(okHttpClient)
                        .build();
                ExtraApi afk_api = retrofit.create(ExtraApi.class);
                Call call = afk_api.add_suggestion("1234567890", dataMaster);
                call.enqueue(new Callback<DataMaster>() {
                    @Override
                    public void onResponse(Call<DataMaster> call, Response<DataMaster> response) {
                        if (response.body() != null) {
                            // Todo bien
                            if (response.body().getData() != null){
                                if (!response.body().getData().isError()){
                                    Toast.makeText(getActivity(), "Your suggestion is waiting for moderation", Toast.LENGTH_LONG).show();
                                    dismiss();
                                }else{
                                    Toast.makeText(getActivity(), "Error :(", Toast.LENGTH_LONG).show();
                                }
                            }else{
                                Toast.makeText(getActivity(), "Error :( ..", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Log.d("ERRORRRRRR", "onResponse: " + t.getMessage());
                        button_procon_save.setEnabled(true);

                    }
                });
            }
        });
    }


    public void llenaSpinner(){
        spinner = v.findViewById(R.id.spinner_pro_con_selection);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.pro_contra,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.simple_spinner_drop_dialog_item);
        spinner.setAdapter(adapter);
    }
}
