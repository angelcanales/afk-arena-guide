package com.mxlapps.app.afk_arenaguide.Views.Sections;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.mxlapps.app.afk_arenaguide.Adapter.RolDefinitionAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.RolDefinitionModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.ExtraViewModel;

import java.util.ArrayList;

public class RolDefinitionActivity extends AppCompatActivity {

    private ExtraViewModel extraViewModel;
    private View rootView;


    @Override
    public void  onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_rol_definition);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_global);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("AFK GUIDE - ROLE DEFINITION");
            getSupportActionBar().setSubtitle("Know the meaning of the role");
        }

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        extraViewModel = ViewModelProviders.of(RolDefinitionActivity.this).get(ExtraViewModel.class);
        fillInfo();

//        Util.initAds(rootView, this, BuildConfig.ADS_ROLE_DEFINITION);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    private void fillInfo() {
        extraViewModel.getRoleDefinitions().observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });
    }


    private void populateRecycler(ArrayList<RolDefinitionModel> rolDefinitionsModels) {
        RecyclerView recyclerView = findViewById(R.id.recycler_rol_definition);
        LinearLayoutManager layoutManager = new LinearLayoutManager(RolDefinitionActivity.this);
        RolDefinitionAdapter adapter = new RolDefinitionAdapter(rolDefinitionsModels);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion) {
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, RolDefinitionActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);

                assert dataMasterResource.data != null;
                ArrayList<RolDefinitionModel> rolDefinitionsArray = dataMasterResource.data.getData().getRol_definition();
                populateRecycler(rolDefinitionsArray);

                break;
            default:
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        setResult(RESULT_OK);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}
