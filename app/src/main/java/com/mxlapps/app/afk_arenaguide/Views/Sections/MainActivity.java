package com.mxlapps.app.afk_arenaguide.Views.Sections;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.NewsModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.AppPreferences;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.ExtraViewModel;
import com.mxlapps.app.afk_arenaguide.Views.Decks.DeckListActivity;
import com.mxlapps.app.afk_arenaguide.Views.Heroes.ListHeroesActivity;
import com.mxlapps.app.afk_arenaguide.Views.LoginFacebookActivity;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mxlapps.app.afk_arenaguide.Views.Heroes.ListHeroesActivity.APP_URL;

public class MainActivity extends AppCompatActivity {

    private ExtraViewModel extraViewModel;
    private static final String TAG = "afkArenaMainActivity";
    private View rootView;
    private ArrayList<NewsModel> newsModel = new ArrayList<>();
    private ArrayList<SectionsModel> sectionsModels = new ArrayList<>();
    String newVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rootView = getWindow().getDecorView().findViewById(android.R.id.content);


//        Util.initAds(rootView, this, BuildConfig.ADS_MAIN);


        // ViewModels
        extraViewModel = ViewModelProviders.of(MainActivity.this).get(ExtraViewModel.class);
        extraViewModel.getNews().observe(MainActivity.this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });

        itemsSections("Profile");
        itemsSections("Heroes");
        itemsSections("Teams");
        itemsSections("Items");
        itemsSections("Rol Definition");
        itemsSections("Faq");
        itemsSections("Contributors");
        itemsSections("About Us");
//        itemsSections("Settings");

        new FetchAppVersionFromGooglePlayStore().execute();


        RecyclerView recyclerView = findViewById(R.id.recyclerview_sections);
        int numberOfColumns = 1;
        SectionsAppAdapter adapter = new SectionsAppAdapter(sectionsModels, MainActivity.this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        adapter.SetOnItemClickListener(new SectionsAppAdapter.OnItemClickListener() {
            @Override
            public void onSectionCardClick(int position) {
                Intent intent = null;
                switch (position){
                    case 0:

                        String userId = AppPreferences.getInstance(MainActivity.this).getUserToken();
                        // Valida que el usuario este logeado
                        if (userId.compareToIgnoreCase("") == 0) {

                            intent = new Intent(MainActivity.this, LoginFacebookActivity.class);

                        }else{
                            intent = new Intent(MainActivity.this, ProfileActivity.class);

                        }
                        break;

                    case 1:
                        intent = new Intent(MainActivity.this, ListHeroesActivity.class);
                        break;

                    case 2:
                        intent = new Intent(MainActivity.this, DeckListActivity.class);
                        break;

                    case 3:
                        intent = new Intent(MainActivity.this, ItemsActivity.class);
                        break;
                    case 4:
                        intent = new Intent(MainActivity.this, RolDefinitionActivity.class);
                        break;
                    case 5:
                        intent = new Intent(MainActivity.this, FaqActivity.class);
                        break;
                    case 6:
                        intent = new Intent(MainActivity.this, ContributorsActivity.class);
                        break;
                    case 7:
                        intent = new Intent(MainActivity.this, AboutUsActivity.class);
                        break;
                    case 8:
                        intent = new Intent(MainActivity.this, SettingsActivity.class);
                        break;
                    default:
                        break;
                }

                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion) {
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, MainActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                // Crea copia de listado de heroes
                switch (opcion) {
                    case 1:
                        newsModel = dataMasterResource.data.getData().getNews();
                        initViewPager(newsModel);
                }
                break;
            default:
                break;
        }
    }

    private void initViewPager(ArrayList<NewsModel> newsModel) {
        // Activa el viewPager
        ViewPager viewPager = findViewById(R.id.pager_main);
        NewsMainAdapter newsMainAdapter = new NewsMainAdapter(getSupportFragmentManager(), newsModel);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(newsMainAdapter);

        viewPager.setClipToPadding(false);
        viewPager.setPadding(50, 0, 50, 0);
        viewPager.setPageMargin(10);
    }

    public void itemsSections(String title){

        SectionsModel section = new SectionsModel();
        section.setTitle(title);
        sectionsModels.add(section);

    }


    class FetchAppVersionFromGooglePlayStore extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {

            //This is the Firebase URL where data will be fetched from
            try {
                String url = "https://play.google.com/store/apps/details?id=" + "com.mxlappsnoads.app.afk_arenaguide" + "&hl=en";
                //Connect to website
                Document document = Jsoup.connect(url).get();

                //Get the logo source of the website
                List<String> img = document.select("span.htlgb").eachText();

                int versionCode = BuildConfig.VERSION_CODE;
                String versionName = BuildConfig.VERSION_NAME;

                Log.d(TAG, "doInBackground version: " + img.get(8));
                Log.d(TAG, "doInBackground version: " + versionName);
                Log.d(TAG, "doInBackground version: " + versionCode);

                if (img.get(8).compareToIgnoreCase(versionName) != 0){
                    Intent rateUsIntent = new Intent(Intent.ACTION_VIEW);
                    rateUsIntent.setData(Uri.parse(APP_URL));
                    startActivity(rateUsIntent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }

            }catch (Exception e){
                return "Error en Request: " + e.getMessage();

            }
            return null;

        }


    }


}


// Adaptador Recycler
class SectionsAppAdapter extends RecyclerView.Adapter<SectionsAppAdapter.SectionsAppViewHolder>{
    private ArrayList<SectionsModel> section;
    private OnItemClickListener mlistener;
    Context ctx;

    TypedArray imgs;



    public void SetOnItemClickListener (OnItemClickListener mlistener) {
        this.mlistener = mlistener;
    }

    public interface OnItemClickListener {
        void onSectionCardClick(int position);
    }

    public SectionsAppAdapter(ArrayList<SectionsModel> section, Context ctx) {
        this.section = section;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public SectionsAppViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sections, parent, false);
        return new SectionsAppViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SectionsAppViewHolder holder, int position) {
        final SectionsModel item = section.get(position);
        holder.textView_item_title.setText(item.getTitle());
//        holder.textView_item_desc.setText(item.getDescription());
        imgs = ctx.getResources().obtainTypedArray(R.array.random_imgs);
        Picasso.get().load(imgs.getResourceId(position, -1)).into(holder.imageView_item_image);
    }

    @Override
    public int getItemCount() {
        return section.size();
    }

    class SectionsAppViewHolder extends RecyclerView.ViewHolder {

        TextView textView_item_title;
        CircleImageView imageView_item_image;
        CardView cardView_section;


        SectionsAppViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_item_title    = itemView.findViewById(R.id.textView_item_title);
            imageView_item_image    = itemView.findViewById(R.id.imageView_item_image);
//            textView_item_desc    = itemView.findViewById(R.id.textView_item_desc);
            cardView_section = itemView.findViewById(R.id.cardView_section);

            cardView_section.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mlistener != null){
                        mlistener.onSectionCardClick(getAdapterPosition());
                    }
                }
            });
        }
    }
}

// Adaptadro ViewPager
class NewsMainAdapter extends FragmentStatePagerAdapter {

    private ArrayList<NewsModel> newsModel;
    public NewsMainAdapter(FragmentManager fm, ArrayList<NewsModel> newsModel){
        super(fm);
        this.newsModel = newsModel;
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("mainAcitivtyprro", "getItem: cambio de fragmento");
        return new NewsElementFragment(newsModel.get(position));
    }
    @Override
    public int getCount() {
        return newsModel.size();
    }


    // Fragmento para las noticias
    public static class NewsElementFragment extends Fragment {
        View v;
        NewsModel newsModel;

        public NewsElementFragment() {
        }

        public NewsElementFragment(NewsModel newsModel) {
            this.newsModel = newsModel;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            v =  inflater.inflate(R.layout.fragment_news_main_activity, container, false);
            ImageView imageNew = v.findViewById(R.id.imageView_newMain);
            LinearLayout contenedor_news = v.findViewById(R.id.contenedor_news);
            if (newsModel != null){
                Picasso.get().load(newsModel.getImage()).into(imageNew);
                contenedor_news.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        NewsInformationBottomSheetFragment newsInformationBottomSheetFragment = new NewsInformationBottomSheetFragment(newsModel);
                        newsInformationBottomSheetFragment.show(getActivity().getSupportFragmentManager(),"deckSaveNewDeckFragment");

                    }
                });
            }

            return v;
        }
    }
}

// MODELO

class SectionsModel {

    private String title;
    private String description;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
