package com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Adapter.DeckCommentsAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.CommentsModel;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.AppPreferences;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.ExtraViewModel;
import com.mxlapps.app.afk_arenaguide.Views.Decks.ShowDeckActivity;
import com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment.CommentsFragment;
import com.mxlapps.app.afk_arenaguide.Views.LoginFacebookActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CommentsFragment extends Fragment {
    View v;
    ArrayList<CommentsModel> commentsModel = new ArrayList<>();
    DeckCommentsAdapter adapter;
    ImageView button_send_comment;
    ImageView imageViewUsuario;
    String token;
    HeroModel hero;
    View rootView;
    private ExtraViewModel extraViewModel;
    EditText textInputEditText_nuevo_comentario;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String heroString = getArguments().getString("hero");
        Gson gson = new Gson();
        hero = gson.fromJson(heroString, HeroModel.class);

        this.commentsModel = hero.getComments();

        rootView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);


        v = inflater.inflate(R.layout.fragment_comments, container, false);

        extraViewModel = ViewModelProviders.of(getActivity()).get(ExtraViewModel.class);

        button_send_comment = v.findViewById(R.id.button_send_comment);
        imageViewUsuario    = v.findViewById(R.id.imageViewUsuario);

        initEvents();

        initRecyclerViewComentarios();

        textInputEditText_nuevo_comentario = v.findViewById(R.id.textInputEditText_nuevo_comentario);


        // Carga la imagen del usuario si ya esta logeado
        token = AppPreferences.getInstance(getActivity()).getUserToken();
        if (token.compareToIgnoreCase("") != 0){
            // Se carga la imagen del usuario en los comentarios
            Integer idUsuario = AppPreferences.getInstance(getActivity()).getUsuario_id();
            String[] urlfixed = BuildConfig.API_BASE_URL.split("api/v2");
            String urlImage = urlfixed[0] + "assets/images/users/user_" + idUsuario + ".jpg";

            Picasso.get().load(urlImage).into(imageViewUsuario);
        }


        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        token = AppPreferences.getInstance(getActivity()).getUserToken();
            if (token.compareToIgnoreCase("") != 0){
                // Se carga la imagen del usuario en los comentarios
                Integer idUsuario = AppPreferences.getInstance(getActivity()).getUsuario_id();
            String[] urlfixed = BuildConfig.API_BASE_URL.split("api/v2");
            String urlImage = urlfixed[0] + "assets/images/users/user_" + idUsuario + ".jpg";

            Picasso.get().load(urlImage).into(imageViewUsuario);
        }
    }

    private void initEvents() {
        button_send_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // En cuanto se hace click, se esconde el teclado
                hideKeyboard(getActivity());



                String comentario = (textInputEditText_nuevo_comentario.getText() == null)? "": textInputEditText_nuevo_comentario.getText().toString();
                if (comentario.compareToIgnoreCase("") == 0){
                    Toast.makeText(getActivity(), "Write your comment", Toast.LENGTH_SHORT).show();
                }else{
                    // Valida que este logeado
                    token = AppPreferences.getInstance(getActivity()).getUserToken();
                    if (token.compareToIgnoreCase("") != 0){
                        // Se escribio un comentario, hay que crear el objeto y enviarlo
                        CommentsModel commentsModel = new CommentsModel();
                        commentsModel.setComment(comentario);
                        commentsModel.setItem_id(hero.getId());
                        commentsModel.setUser(String.valueOf(AppPreferences.getInstance(getActivity()).getUsuario_id()));
                        commentsModel.setSection("hero_detail");
                        extraViewModel.createComment(commentsModel).observe(getActivity(), new Observer<Resource<DataMaster>>() {
                            @Override
                            public void onChanged(Resource<DataMaster> dataMasterResource) {
                                procesaRespuesta(dataMasterResource, 2);
                            }
                        });
                    }else{
                        Intent intent = new Intent(getActivity(), LoginFacebookActivity.class);
                        startActivityForResult(intent, 1001);
                    }
                }
            }
        });
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion) {
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, getActivity());
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                // Crea copia de listado de heroes
                switch (opcion) {
                    case 1:
                        // Carga informacion del deck con los comentarios
                        this.commentsModel = dataMasterResource.data.getData().getComments();
                        adapter.notifyDataSetChanged();
                        break;
                    case 2:
                        // Respuesta al crear un comentario nuevo

                        if (dataMasterResource.data.getData().isError()){
                            Toast.makeText(getActivity(), dataMasterResource.data.getData().getMsg(), Toast.LENGTH_SHORT).show();
                        }else{
                            this.commentsModel = dataMasterResource.data.getData().getComments();
                            initRecyclerViewComentarios();
                            textInputEditText_nuevo_comentario.setText("");
                            Toast.makeText(getActivity(), "Comments Added!", Toast.LENGTH_SHORT).show();
                        }


                        break;
                    case 3:
                        // Repsusta al enviar la votacion
                        Toast.makeText(getActivity(), "Thank you for your vote!", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void initRecyclerViewComentarios() {
        if (commentsModel.size() > 0){
            // Cardview is alwasy invisible
            RecyclerView recycler_comments = v.findViewById(R.id.recycler_commentsDetailheroe);

            int numberOfColumns = 1;
            adapter = new DeckCommentsAdapter(commentsModel, getActivity(), 1);
            recycler_comments.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
            recycler_comments.setNestedScrollingEnabled(false);
            recycler_comments.setHasFixedSize(true);
            recycler_comments.setAdapter(adapter);
        }else{
            Log.d("test", "initRecyclerViewComentarios: ----------------------------><");
        }
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
