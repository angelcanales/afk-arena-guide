package com.mxlapps.app.afk_arenaguide.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HeroesFastAdapter extends RecyclerView.Adapter<HeroesFastAdapter.HeroViewHolder> {

    private ArrayList<HeroModel> heroModels;
    private OnItemClickListener mlistener;
    Context ctx;
    int modo = 1;


    public void SetOnItemClickListener (OnItemClickListener mlistener) {
        this.mlistener = mlistener;
    }


    public interface OnItemClickListener {
        void onHeroCardClick(int position);
    }



    public HeroesFastAdapter(ArrayList<HeroModel> heroModels, Context context, int modo) {
        this.heroModels = heroModels;
        this.ctx = context;
        this.modo = modo;
    }

    @NonNull
    @Override
    public HeroesFastAdapter.HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hero_deck, parent, false);
        return new HeroViewHolder(view, mlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull final HeroesFastAdapter.HeroViewHolder holder, int position) {
        HeroModel hero = heroModels.get(position);
        Picasso.get().load(hero.getSmallImage()).into(holder.smallImage);
    }

    @Override
    public int getItemCount() {
        return heroModels.size();
    }

    public class HeroViewHolder extends RecyclerView.ViewHolder {
        TextView textView_tier;
        ImageView smallImage;
        CardView cardView_hero_item;

        public HeroViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            smallImage = itemView.findViewById(R.id.imageView_hero_image);
//            textView_tier = itemView.findViewById(R.id.textView_tier);
            cardView_hero_item = itemView.findViewById(R.id.cardView_section_item);
            cardView_hero_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        listener.onHeroCardClick(getAdapterPosition());
                    }
                }
            });
        }
    }

}
