package com.mxlapps.app.afk_arenaguide.Adapter;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment.CommentsFragment;
import com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment.InfoFragment;
import com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment.LoreFragment;
import com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment.ProsConsFragment;
import com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment.SkillsFragment;
import com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment.TierDataFragment;

public class HeroDetailAdapter extends FragmentStatePagerAdapter {
    HeroModel hero;
    public HeroDetailAdapter(FragmentManager fm, HeroModel hero){
        super(fm);
        this.hero = hero;
    }
    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        String heroString =  gson.toJson(hero);
        bundle.putString("hero", heroString);

        switch (position){
            case 0:
                CommentsFragment commentsFragment = new CommentsFragment();
                commentsFragment.setArguments(bundle);
                return commentsFragment;
            case 1:
                InfoFragment info = new InfoFragment();
                info.setArguments(bundle);
                return info;
            case 2:
                SkillsFragment skillsFragment = new SkillsFragment();
                skillsFragment.setArguments(bundle);
                return skillsFragment;
            case 3:
                TierDataFragment tierDataFragment = new TierDataFragment();
                tierDataFragment.setArguments(bundle);
                return tierDataFragment;
            case 4:
                ProsConsFragment prosConsFragment = new ProsConsFragment();
                prosConsFragment.setArguments(bundle);
                return prosConsFragment;
            case 5:
                LoreFragment loreFragment = new LoreFragment();
                loreFragment.setArguments(bundle);
                return loreFragment;
        }
        return null;
    }
    @Override
    public int getCount() {
        return 6;
    }
    @Override    public CharSequence getPageTitle(int position) {        switch (position){
        case 0: return "Comments";
        case 1: return "Info";
        case 2: return "Skills";
        case 3: return "Tier Data";
        case 4: return "Pros-Cons";
        case 5: return "Lore";
        default: return null;
    }
    }
}
