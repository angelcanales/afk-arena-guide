package com.mxlapps.app.afk_arenaguide.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.ArtifacRecomendationModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ArtifactRecomendationAdapter extends RecyclerView.Adapter<ArtifactRecomendationAdapter.SynergiViewHolder>{

    private ArrayList<ArtifacRecomendationModel> synergi;

    public ArtifactRecomendationAdapter(ArrayList<ArtifacRecomendationModel> synergi) {
        this.synergi = synergi;
    }

    @NonNull
    @Override
    public SynergiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artifact_recomendation, parent, false);
        return new SynergiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SynergiViewHolder holder, int position) {
        final ArtifacRecomendationModel synergiModel = synergi.get(position);
//        holder.textview_synergi_hero_name.setText(synergiModel.getName());
        if (synergiModel.getIcon().compareToIgnoreCase("") != 0)
            Picasso.get().load(synergiModel.getIcon()).into(holder.imageView_artifact_icon);
    }

    @Override
    public int getItemCount() {
        return synergi.size();
    }

    class SynergiViewHolder extends RecyclerView.ViewHolder {

//        TextView textview_synergi_hero_name;
        ImageView imageView_artifact_icon;

        SynergiViewHolder(@NonNull View itemView) {
            super(itemView);
//            textview_synergi_hero_name    = itemView.findViewById(R.id.textview_synergi_hero_name);
            imageView_artifact_icon = itemView.findViewById(R.id.imageView_artifact_icon);
        }
    }
}
