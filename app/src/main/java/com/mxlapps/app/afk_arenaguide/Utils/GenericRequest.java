package com.mxlapps.app.afk_arenaguide.Utils;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.mxlapps.app.afk_arenaguide.Model.StrengthWeaknessModel;
import com.mxlapps.app.afk_arenaguide.Request.Data;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.GenericApi;
import com.mxlapps.app.afk_arenaguide.ViewModel.GenericViewModel;
import com.mxlapps.app.afk_arenaguide.ViewModel.HeroViewModel;
import com.mxlapps.app.afk_arenaguide.ViewModel.UserViewModel;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public  class GenericRequest {

    private String TAG = "clase generica";
    String URL_SERVER_DOCDIGITALES = "http://bb21a9b5.ngrok.io/afkapi/api/v2/";
    private StrengthWeaknessModel strengthWeaknessModel;
    private View rootView;
    HeroViewModel heroViewModel;
    UserViewModel userViewModel;
    GenericViewModel genericViewModel;
    Context ctx;


//
//    public GenericRequest(StrengthWeaknessModel strengthWeaknessModel, Context ctx) {
//        this.strengthWeaknessModel = strengthWeaknessModel;
//        this.ctx = ctx;
//        createStrength();
//    }


    public void createStrength(StrengthWeaknessModel strengthWeaknessModel) {
        // TODO refactor de retrofit a modo generico
        final String[] status = {"inicio"};
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_SERVER_DOCDIGITALES)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        GenericApi genericApi = retrofit.create(GenericApi.class);

        Data data = new Data();
        data.setStrengthWeakness(strengthWeaknessModel);
        DataMaster dataMaster = new DataMaster();
        dataMaster.setData(data);

        Call<DataMaster> call = genericApi.createStrengthWeakness(dataMaster);
        call.enqueue(new Callback<DataMaster>() {
            @Override
            public void onResponse(Call<DataMaster> call, final Response<DataMaster> response) {

                if (!response.isSuccessful()) {
                    Log.i("onResponse", "No se recibio la llave error");
                    status[0] = "error";
                }
                if (response.body().getData().isError()){
                    Log.i("onResponse", response.body().getData().getMsg());
                    status[0] = "error en api";
                }
                status[0] = "todo bien";

                Log.d(TAG, "onResponse: Todo bien");
            }

            @Override
            public void onFailure(Call<DataMaster> call, Throwable t) {
                Log.d("onResponse", "onResponse: " + t.getMessage());
            }
        });

        status[0] = "esperando";
//        return "idd";
    }



}
