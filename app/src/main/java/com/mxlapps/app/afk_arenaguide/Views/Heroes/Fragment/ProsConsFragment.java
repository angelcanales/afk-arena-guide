package com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Adapter.ProsConsAdapter;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Utils.AppPreferences;
import com.mxlapps.app.afk_arenaguide.Views.LoginFacebookActivity;

import java.util.Objects;

public class ProsConsFragment extends Fragment {
    private View v;
    private HeroModel hero;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        assert getArguments() != null;
        String heroString = getArguments().getString("hero");
        Gson gson = new Gson();
        this.hero =  gson.fromJson(heroString, HeroModel.class);
        v = inflater.inflate(R.layout.fragment_proscons, container, false);
        fillData();
        eventsAddNewSuggestion();
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            String token = AppPreferences.getInstance(getActivity()).getUserToken();
            Boolean tap_target = AppPreferences.getInstance(getActivity()).getTapTarget();

            if (token.compareToIgnoreCase("") == 0 && !tap_target){
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 2s
                        TapTargetView.showFor(Objects.requireNonNull(getActivity()),                 // `this` is an Activity
                                TapTarget.forView(v.findViewById(R.id.floating_btn_add_pro_con), "Add your Suggestions", "Now you can share your opinion about the strengths and weaknesses of your favorite heroes.\n" +
                                        "Just select the type of suggestion you want to give, describe it and send it. We will moderate the suggestions you send.")
                                        // All options below are optional
//                        .outerCircleColor(R.color.red)      // Specify a color for the outer circle
                                        .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
//                        .targetCircleColor(R.color.white)   // Specify a color for the target circle
                                        .titleTextSize(20)                  // Specify the size (in sp) of the title text
//                        .titleTextColor(R.color.white)      // Specify the color of the title text
                                        .descriptionTextSize(13)            // Specify the size (in sp) of the description text
//                        .descriptionTextColor(R.color.red)  // Specify the color of the description text
//                        .textColor(R.color.blue)            // Specify a color for both the title and description text
                                        .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                        .dimColor(R.color.colorPrimaryDark)            // If set, will dim behind the view with 30% opacity of the given color
                                        .drawShadow(true)                   // Whether to draw a drop shadow or not
                                        .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                        .tintTarget(true)                   // Whether to tint the target view's color
                                        .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                                        .icon(getResources().getDrawable(R.drawable.ic_add))                     // Specify a custom drawable to draw as the target
                                        .targetRadius(60),                  // Specify the target radius (in dp)
                                new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                                    @Override
                                    public void onTargetClick(TapTargetView view) {
                                        super.onTargetClick(view);      // This call is optional
                                        Log.d("oooooo", "onTargetClick: ");
                                    }
                                });
                        AppPreferences.getInstance(getActivity()).setTapTarget(true); // Ya no vuelve a mostrar el taptarget
                    }
                }, 1000);

            }
        }
    }

    private void fillData() {

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview_pros_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        ProsConsAdapter adapter = new ProsConsAdapter(hero.getStrengths(),1 , getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        RecyclerView recyclerView2 = v.findViewById(R.id.recyclerview_con_list);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        ProsConsAdapter adapter2 = new ProsConsAdapter(hero.getWeaknesses(), 2, getActivity());
        recyclerView2.setLayoutManager(layoutManager2);
        recyclerView2.setAdapter(adapter2);

    }

    private void eventsAddNewSuggestion() {
        FloatingActionButton floating_btn_add_pro_con = v.findViewById(R.id.floating_btn_add_pro_con);
        floating_btn_add_pro_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Valida si esta logeado
                if (AppPreferences.getInstance(getActivity()).getName().compareToIgnoreCase("") == 0){
                    Intent intent = new Intent(getActivity(), LoginFacebookActivity.class);
                    startActivityForResult(intent, 1001);
                }else {
                    BottomSheetStrengthWeakness bottomSheetStrengthWeakness = new BottomSheetStrengthWeakness(hero);
                    bottomSheetStrengthWeakness.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "efefe");
                }
            }
        });
    }

}
