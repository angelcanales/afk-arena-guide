package com.mxlapps.app.afk_arenaguide.Views.Sections;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.mxlapps.app.afk_arenaguide.Adapter.ContributorsAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.ContributorModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.SpacesItemDecoration;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.ExtraViewModel;

import java.util.ArrayList;

public class ContributorsActivity extends AppCompatActivity {

    ExtraViewModel extraViewModel;
    View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributors);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_global);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("AFK GUIDE - Contributors");
        }

        Util.setContext(this);

        // RootView
        rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        extraViewModel = ViewModelProviders.of(ContributorsActivity.this).get(ExtraViewModel.class);



        extraViewModel.getContributors().observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });

//        Util.initAds(rootView, this, BuildConfig.ADS_CONTRIBUTORS);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion){
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status){
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, ContributorsActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                // Se obtiene la informacion del heroe
                switch (opcion){
                    case 1:
                        responseGetContributors(dataMasterResource);
//                        responseGuildList(dataMasterResource);
                        break;
                }


                break;
            default:
                break;
        }
    }

    private void responseGetContributors(Resource<DataMaster> dataMasterResource) {
        RecyclerView recyclerView = findViewById(R.id.recyclerView_contributors);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ContributorsActivity.this);
        int numberOfColumns = 1;

        assert dataMasterResource.data != null;
        final ArrayList<ContributorModel> contributorModels = dataMasterResource.data.getData().getContributors();

        ContributorsAdapter adapter = new ContributorsAdapter(contributorModels, ContributorsActivity.this);
        recyclerView.setHasFixedSize(false);
//        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLayoutManager(new GridLayoutManager(ContributorsActivity.this, numberOfColumns));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void finish() {
        super.finish();
        setResult(RESULT_OK);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
