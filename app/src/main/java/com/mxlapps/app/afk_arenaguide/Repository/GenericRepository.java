package com.mxlapps.app.afk_arenaguide.Repository;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.GenericApi;
import com.mxlapps.app.afk_arenaguide.Service.NetworkBoundResource;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Service.RetrofitRequest;

import retrofit2.Call;

public class GenericRepository {

    private GenericApi apiService;
    private static GenericRepository instance;
    private Context context;

    public static GenericRepository getInstance(Context context){
        if(instance == null){
            instance = new GenericRepository(context);
        }
        return instance;
    }
    public GenericRepository(Context context) {
        apiService = RetrofitRequest.getInstance().create(GenericApi.class);
        this.context = context;
    }


    public LiveData<Resource<DataMaster>> createStrengthWeakness(final DataMaster dataMaster) {
        return new NetworkBoundResource<DataMaster, DataMaster>() {
            @NonNull
            @Override
            protected Call<DataMaster> createCallRetrofit() {
                return apiService.createStrengthWeakness(dataMaster);
            }
        }.getAsLiveData();
    }
}
