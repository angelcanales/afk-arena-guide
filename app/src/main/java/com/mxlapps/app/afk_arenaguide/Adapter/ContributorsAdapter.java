package com.mxlapps.app.afk_arenaguide.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.ContributorModel;
import com.mxlapps.app.afk_arenaguide.Model.GuildModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ContributorsAdapter extends RecyclerView.Adapter<ContributorsAdapter.GuildViewHolder>{

    private ArrayList<ContributorModel> contributorModels;
    private ArrayList<GuildModel> contributorModelsFull;
    private OnItemClickListener mlistener;
    Context ctx;


    public void SetOnItemClickListener (OnItemClickListener mlistener) {
        this.mlistener = mlistener;
    }


    public interface OnItemClickListener {
        void onGuildCardClick(int position);
    }



    public ContributorsAdapter(ArrayList<ContributorModel> contributorModels, Context context) {
        this.contributorModels = contributorModels;
        this.ctx = context;
//        contributorModelsFull = new ArrayList<>(heroModels);
    }

    @NonNull
    @Override
    public ContributorsAdapter.GuildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contributor, parent, false);
        return new GuildViewHolder(view, mlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContributorsAdapter.GuildViewHolder holder, int position) {

        final ContributorModel contributor = contributorModels.get(position);
        holder.contributor_name.setText(contributor.getName());
        holder.contributor_type_contribution.setText(contributor.getType_contribution());
        holder.contributor_desc.setText(contributor.getDesc());
        holder.contributor_link.setText(contributor.getButtontext());

        Picasso.get().load(contributor.getImagelink()).into(holder.imageView_icono_contribuyente);


        holder.contributor_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String urlFinal = contributor.getLink();
                intent.setData(Uri.parse(urlFinal));
                ctx.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return contributorModels.size();
    }

    public class GuildViewHolder extends RecyclerView.ViewHolder {

        TextView contributor_name;
        TextView contributor_type_contribution;
        TextView contributor_desc;
        Button contributor_link;
        ImageView imageView_icono_contribuyente;


        CardView cardView_contributors;

        public GuildViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            contributor_name    = itemView.findViewById(R.id.contributor_name);
            contributor_type_contribution     = itemView.findViewById(R.id.contributor_type_contribution);
            contributor_desc= itemView.findViewById(R.id.contributor_desc);
            contributor_link= itemView.findViewById(R.id.contributor_link);
            imageView_icono_contribuyente= itemView.findViewById(R.id.imageView_icono_contribuyente);
            cardView_contributors = itemView.findViewById(R.id.cardView_contributors);

            cardView_contributors.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        listener.onGuildCardClick(getAdapterPosition());
                    }
                }
            });
        }
    }
}
