package com.mxlapps.app.afk_arenaguide.Views.Sections;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.mxlapps.app.afk_arenaguide.Adapter.HeroesFastAdapter;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;

import java.util.ArrayList;

public class HeroFastListFragment extends BottomSheetDialogFragment {
    String TAG = "heroFragmenteFast";
    View v;
    ArrayList<HeroModel> heroModels = new ArrayList<>();

    public static HeroFastListFragment newInstance() {
        return new HeroFastListFragment();
    }

    public interface CerrarTodo{
        public void cierrate(HeroModel heroModel);
    }

    public HeroFastListFragment.CerrarTodo listener;

    public void SetOnItemClickListener (HeroFastListFragment.CerrarTodo mlistener) {
        this.listener = mlistener;
    }


    public HeroFastListFragment() {
    }

    public HeroFastListFragment(ArrayList<HeroModel> heroModels) {
        this.heroModels = heroModels;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_herofastlist, container, false);
        Log.d("bottomSheet", "onCreateView: ");
        initRecyclerView(heroModels);
        return v;
    }


    private void initiViews() {

    }

    private void initRecyclerView(final ArrayList<HeroModel> heroModelsInternal) {
        RecyclerView recyclerView = v.findViewById(R.id.herofastRecycler);
        // Recibe un arrayList de productosy la bandera si se quiere mostrar/ocultar el checkbox
        int numberOfColumns = 4;
        HeroesFastAdapter adapter = new HeroesFastAdapter(heroModelsInternal, getActivity(), 1);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setAdapter(adapter);



        adapter.SetOnItemClickListener(new HeroesFastAdapter.OnItemClickListener() {
            @Override
            public void onHeroCardClick(int position) {
                Log.d(TAG, "onHeroCardClick: se hizo click en el recycler un heroe");
                listener.cierrate(heroModelsInternal.get(position));
                dismiss();
            }
        });
    }


}