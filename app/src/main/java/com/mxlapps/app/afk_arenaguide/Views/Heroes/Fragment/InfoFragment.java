package com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Adapter.ArtifactRecomendationAdapter;
import com.mxlapps.app.afk_arenaguide.Adapter.SynergiesAdapter;
import com.mxlapps.app.afk_arenaguide.Model.ArtifacRecomendationModel;
import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.Model.SynergiModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class InfoFragment extends Fragment {

    HeroModel hero;
    View v;
    String TAG = "kokookook";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String heroString = getArguments().getString("hero");
        Gson gson = new Gson();
        this.hero =  gson.fromJson(heroString, HeroModel.class);
        v  = inflater.inflate(R.layout.fragment_info, container, false);

        fillInfo();

        return v;
    }


    private void fillInfo() {
        // busca el imageview
        // First row
        TextView textView_race_text     = v.findViewById(R.id.textView_race_text);
        TextView textView_rarity_text   = v.findViewById(R.id.textView_rarity_text);
        TextView textView_class__text   = v.findViewById(R.id.textView_class__text);
        TextView textView_primary_role      = v.findViewById(R.id.textView_primary_role);
        TextView textView_secondary_role      = v.findViewById(R.id.textView_secondary_role);

        String racename  = hero.getRace_name();
        racename = racename.substring(0,1).toUpperCase() + racename.substring(1).toLowerCase();

        textView_race_text.setText(racename);
        textView_rarity_text.setText(hero.getRarity());
        textView_class__text.setText(hero.getClasse());
        textView_primary_role.setText(hero.getPrimary_rol());
        textView_secondary_role.setText(hero.getSecondary_rol());

        switch (hero.getRarity()){
            case "Legendary+":
                textView_rarity_text.setTextColor(ContextCompat.getColor(getActivity(), R.color.legendary));
                break;
            case "Common":
                textView_rarity_text.setTextColor(ContextCompat.getColor(getActivity(), R.color.common));
                break;
            case "Ascended":
                textView_rarity_text.setTextColor(ContextCompat.getColor(getActivity(), R.color.ascend));
                break;
        }

        // Secondrow
        TextView textView_detail_positcion     = v.findViewById(R.id.textView_detail_positcion);
        TextView textView_detail_type   = v.findViewById(R.id.textView_detail_type);
        TextView textView_detail_union      = v.findViewById(R.id.textView_detail_union);

        textView_detail_positcion.setText(hero.getPosition());
        textView_detail_type.setText(hero.getType());
        textView_detail_union.setText(hero.getUnion());

        // Tird row
        TextView textView_detail_is_food      = v.findViewById(R.id.textView_detail_is_food);
        if (hero.getSkills().size() == 4){
            textView_detail_is_food.setText("No");
        }else{
            if ((hero.getName().compareToIgnoreCase("Saveas") == 0) || hero.getName().compareToIgnoreCase("Arden") == 0){
                textView_detail_is_food.setText("No");
            }else{
                textView_detail_is_food.setText("Yes");
            }
        }


        TextView textView_detail_num_skills      = v.findViewById(R.id.textView_detail_num_skills);
        TextView textView_detail_introduction      = v.findViewById(R.id.textView_detail_introduction);
        TextView textView_detail_strengths      = v.findViewById(R.id.textView_detail_strengths);
        TextView textView_detail_weaknesses      = v.findViewById(R.id.textView_detail_weaknesses);


        textView_detail_num_skills.setText(String.valueOf(hero.getSkills().size()));

        if (hero.getStrengths().size() > 0){
            String num_str = String.valueOf(hero.getStrengths().size());
            textView_detail_strengths.setText(num_str);
        }else{
            textView_detail_strengths.setText("0");
        }

        if (hero.getWeaknesses().size() > 0){
            String num_str = String.valueOf(hero.getWeaknesses().size());
            textView_detail_weaknesses.setText(num_str);
        }else{
            textView_detail_weaknesses.setText("0");
        }

        textView_detail_introduction.setText(hero.getIntroduction());

        //////////////////////////////////////////
        /// PInta recycler de synergia
        RecyclerView recyclerView = v.findViewById(R.id.recycler_Synergies);
        ConstraintLayout constraintLayout_Synergies = v.findViewById(R.id.constraintLayout_Synergies);
        // Recibe un arrayList de productosy la bandera si se quiere mostrar/ocultar el checkbox
        ArrayList<SynergiModel> synergiModels = new ArrayList<>();

        if (hero.getSynergy().compareToIgnoreCase("") != 0){
            String[] syn = hero.getSynergy().split(",");
            for (int x = 0; x < syn.length; x++){
                SynergiModel model = new SynergiModel();
                model.setIcon("https://www.mxl-apps.com/afkapi/assets/heroes/icons/" + syn[x].replaceAll("\\s+","") + ".jpg");
                synergiModels.add(model);
            }
            SynergiesAdapter adapter = new SynergiesAdapter(synergiModels);
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));
            recyclerView.setAdapter(adapter);
        }else{
            constraintLayout_Synergies.setVisibility(View.GONE);
        }

        //////////////////////////////////////////


        //////////////////////////////////////////
        /// PInta recycler de articafts
        RecyclerView recyclerViewArtifacts = v.findViewById(R.id.recycler_arficats_recomendations);
        ConstraintLayout constraintLayout_artifacts_recomendations = v.findViewById(R.id.constraintLayout_artifacts_recomendations);

        // Recibe un arrayList de productosy la bandera si se quiere mostrar/ocultar el checkbox
        ArrayList<ArtifacRecomendationModel> artifactsModels = new ArrayList<>();

        if (hero.getArtifact().compareToIgnoreCase("") != 0){
            String[] artf = hero.getArtifact().split(",");
            for (int x = 0; x < artf.length; x++){
                ArtifacRecomendationModel model = new ArtifacRecomendationModel();
                model.setIcon(getArtifactImage(artf[x]));
                artifactsModels.add(model);
            }
            ArtifactRecomendationAdapter adapterArtifacts = new ArtifactRecomendationAdapter(artifactsModels);
            recyclerViewArtifacts.setLayoutManager(new GridLayoutManager(getActivity(), 5));
            recyclerViewArtifacts.setAdapter(adapterArtifacts);
        }else{
            constraintLayout_artifacts_recomendations.setVisibility(View.GONE);
        }
        //////////////////////////////////////////


        rarity(hero.getRarity());
        raceIcon(racename);
        classe(hero.getClasse());
    }

    public String getArtifactImage(String artifact){

        String base = "https://www.mxl-apps.com/afkapi/assets/heroes/artifacts/";

        if (artifact.compareToIgnoreCase("Dura\'s Grace") == 0){
            return base + "/grace.jpg";
        }else if (artifact.compareToIgnoreCase("Dura\'s Eye") == 0){
            Log.d(TAG, "getArtifactImage: aquiii");
            return base + "/eye.jpg";
        }else if (artifact.compareToIgnoreCase("Dura\'s Call") == 0){
            return base + "/call.jpg";
        }else if (artifact.compareToIgnoreCase("Dura\'s Drape") == 0){
            return base + "/drape.jpg";
        }else if (artifact.compareToIgnoreCase("Dura\'s Blade") == 0){
            return base + "/blade.jpg";
        }else if (artifact.compareToIgnoreCase("Dura\'s Chalice of Vitality") == 0){
            return base + "/chalice.jpg";
        }else if (artifact.compareToIgnoreCase("Dura\'s Conviction") == 0){
            return base + "/conviction.jpg";
        }else{
            return "";
        }
    }




    public void raceIcon(String type) {
        ImageView imageView_race = v.findViewById(R.id.imageView_race);
        switch (type){
            case "Lightbearer":
                Picasso.get().load(R.drawable.humen_i).into(imageView_race);
                break;
            case "Mauler":
                Picasso.get().load(R.drawable.orc_i).into(imageView_race);
                break;
            case "Wilder":
                Picasso.get().load(R.drawable.elf_i).into(imageView_race);
                break;
            case "Graveborn":
                Picasso.get().load(R.drawable.undead_i).into(imageView_race);
                break;
            case "Celestial":
                Picasso.get().load(R.drawable.celestials).into(imageView_race);
                break;
            case "Hypogean":
                Picasso.get().load(R.drawable.hypogean).into(imageView_race);
                break;
        }
    }


    private void classe(String classe)  {
        ImageView imageView_class = v.findViewById(R.id.imageView_class);
        switch (classe){
            case "Intelligence":
                Picasso.get().load(R.drawable.intelligence).into(imageView_class);
                break;
            case "Agility":
                Picasso.get().load(R.drawable.agility).into(imageView_class);
                break;
            case "Strength":
                Picasso.get().load(R.drawable.strength).into(imageView_class);
                break;
            default:
                Log.d("odjosjodjodjodjo", "classe: " + classe);
                break;
        }
    }


    private void rarity(String rarity) {
        ImageView imageView_rarity = v.findViewById(R.id.imageView_rarity);
        switch (rarity){
            case "Legendary+":
                Picasso.get().load(R.drawable.legendary).into(imageView_rarity);
                break;
            case "Ascended":
                Picasso.get().load(R.drawable.ascended_icon).into(imageView_rarity);
                break;
            case "Common":
                Picasso.get().load(R.drawable.common).into(imageView_rarity);
                break;
            default:
                Log.d("odjosjodjodjodjo", "rarity: " + rarity);
                break;
        }
    }

}
