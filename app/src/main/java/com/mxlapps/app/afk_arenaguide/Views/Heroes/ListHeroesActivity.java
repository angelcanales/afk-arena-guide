package com.mxlapps.app.afk_arenaguide.Views.Heroes;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.navigation.NavigationView;
import com.mxlapps.app.afk_arenaguide.Adapter.HeroesAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.Model.ListHeroesModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.Data;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.HeroViewModel;
import com.mxlapps.app.afk_arenaguide.Views.Sections.AboutUsActivity;
import com.mxlapps.app.afk_arenaguide.Views.Sections.ContributorsActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.view.Gravity.RIGHT;

public class ListHeroesActivity extends AppCompatActivity {

    private Boolean isPrimeraVezPrimaryRole = true;
    private Boolean isPrimeraVezSecondaryRole = true;
    public static String FACEBOOK_URL = "https://www.facebook.com/AFK-Arena-Guide-449688835791805";
    public static String APP_URL = "https://play.google.com/store/apps/details?id=com.mxlappsnoads.app.afk_arenaguide";
    public static String FACEBOOK_PAGE_ID = "449688835791805";
    private InterstitialAd interstitial;
    private int AD_INTERSTITIAL = 0;

    private static final String TAG = "afkArenaMainActivity";
    private ArrayList<HeroModel> heroModels;
    private View rootView;

    // Variables Strings

    private String PRIMARY_ROL = "All";
    private String SECONDARY_ROL = "All";
    private String CLASSE_AGILITY = "Agility";
    private String CLASSE_INTELLIGENCE = "Intelligence";
    private String CLASSE_STRENGTH = "Strength";

    private String RARITY = "All";
    private String UNION = "All";
    private String RACE_NAME = "All";
    private String POSITION = "All";
    private String CLASSE = "All";
    private String RACE_NAME_WILDERS = "Wilder";
    private String RACE_NAME_MAULERS = "Mauler";
    private String RACE_NAME_LIGHTBEARERS = "Lightbearer";
    private String RACE_NAME_HYPOGEAN = "Hypogean";
    private String RACE_NAME_CELESTIAL = "Celestial";
    private String RACE_NAME_GRAVEBORN = "Graveborn";

    // Tables
    private String GAME_LEVEL_EARLY = "tier_list_earlies";
    private String GAME_LEVEL_MID   = "tier_list_mids";
    private String GAME_LEVEL_LATE  = "tier_list_lates";
    // Sections
    private String OVERALL  = "overall";
    private String PVP      = "pvp";
    private String PVE      = "pve";
    private String LAB      = "lab";
    private String WRIZZ    = "wrizz";
    private String SOREN    = "soren";

    // Base variables
    private String SECTION      = "overall";
    private String GAME_LEVEL   = "tier_list_earlies";

    Toolbar toolbar;

    private NavigationView navigationView;
    private HeroViewModel heroViewModel;
    DrawerLayout drawer;

    HeroesAdapter adapterRecyclerHeroes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_heroes);

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        drawer = findViewById(R.id.coordinatorLayout_listado_heroes);

        toolbar = findViewById(R.id.toolbar_hero_list);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("Early Lvl 1 - 60");
            getSupportActionBar().setSubtitle("Overall");
        }

        // ViewModels
        heroViewModel = ViewModelProviders.of(ListHeroesActivity.this).get(HeroViewModel.class);
        eventsRightDrawer();
        requestCargarListaDeHeroes();
        initFilter();

//        Util.initAds(rootView, this, BuildConfig.AD_LIST);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tier_hero_list_toolbar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
            case R.id.menu_contributors:
                startActivity(new Intent(ListHeroesActivity.this, ContributorsActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.menu_filtro:
                Log.d(TAG, "onOptionsItemSelected: ji");
                drawer.openDrawer(RIGHT);
                break;
            case R.id.menu_about_us:
                startActivity(new Intent(ListHeroesActivity.this, AboutUsActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.menu_facebook:
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + FACEBOOK_PAGE_ID));
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } catch(Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_URL)));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                break;
            case R.id.menu_rateus:
                Intent rateUsIntent = new Intent(Intent.ACTION_VIEW);
                rateUsIntent.setData(Uri.parse(APP_URL));
                startActivity(rateUsIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: " + AD_INTERSTITIAL);
        Util.stopLoading(rootView);
        AD_INTERSTITIAL++;
        if (AD_INTERSTITIAL == 2) {
            //displayInterstitial();
            AD_INTERSTITIAL = 0;
        }

        if (requestCode == 1001){
            Log.d(TAG, "onActivityResult: tyfuhijhgjhgjhkjgtjhjgyhjhkj");
        }

    }

    private void eventsRightDrawer() {
        // Eventos en drawer derecho - filtros, etc
        drawer = findViewById(R.id.coordinatorLayout_listado_heroes);
        navigationView = findViewById(R.id.nav_view_filter);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return false;
            }
        });
    }







    private void initRecyclerView(final ArrayList<HeroModel> heroModelsInternal) {
        RecyclerView recyclerView = findViewById(R.id.recyclerview_hero_list);
        // Recibe un arrayList de productosy la bandera si se quiere mostrar/ocultar el checkbox
        int numberOfColumns = 5;
        adapterRecyclerHeroes = new HeroesAdapter(heroModelsInternal, ListHeroesActivity.this, 1);
        recyclerView.setLayoutManager(new GridLayoutManager(ListHeroesActivity.this, numberOfColumns));
        recyclerView.setAdapter(adapterRecyclerHeroes);
        adapterRecyclerHeroes.SetOnItemClickListener(new HeroesAdapter.OnItemClickListener() {
            @Override
            public void onHeroCardClick(int position) {
                Util.startLoading(rootView);
                Intent intent = new Intent(ListHeroesActivity.this, HeroDetailV2.class);
                intent.putExtra("hero_id", heroModelsInternal.get(position).getId());
                startActivityForResult(intent, 50);
            }

        });


    }



    private void procesaRespuesta(Resource<DataMaster> dataMasterResource) {
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, ListHeroesActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                // Crea copia de listado de heroes
                heroModels = dataMasterResource.data.getData().getHeroModels();
//                adapterRecyclerHeroes.notifyDataSetChanged();
                initListadoHeroes();
                break;
            default:
                break;
        }
    }

    private void initListadoHeroes() {
        initRecyclerView(heroModels);
//        initFilter();
    }

    private void initFilter() {
        View parentView = navigationView.getHeaderView(0);
        // Spinners de roles
        String[] roles=new String[]{"All","Tank","Regen", "Control", "Assassin", "Continuous Damage", "Burst Damage","AoE", "Debuffer", "Buffer"};

        final Spinner primary_role      = parentView.findViewById(R.id.spinner_primary_role);
        final Spinner secondary_role    = parentView.findViewById(R.id.spinner_secondary_role);

        ArrayAdapter<String> spinnerRoles =
                new ArrayAdapter<>(ListHeroesActivity.this,android.R.layout.simple_spinner_item, roles);
        spinnerRoles.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        primary_role.setAdapter(spinnerRoles);
        secondary_role.setAdapter(spinnerRoles);

        primary_role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (isPrimeraVezPrimaryRole){ isPrimeraVezPrimaryRole = false; return;}
                if (primary_role.getSelectedItem().toString().compareToIgnoreCase("ALL") != 0){
                    PRIMARY_ROL = primary_role.getSelectedItem().toString();
                    requestCargarListaDeHeroes();
                }else{
                    PRIMARY_ROL ="All";
                    requestCargarListaDeHeroes();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        secondary_role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (isPrimeraVezSecondaryRole){ isPrimeraVezSecondaryRole = false; return;}
                if (secondary_role.getSelectedItem().toString().compareToIgnoreCase("ALL") != 0){
                    SECONDARY_ROL = secondary_role.getSelectedItem().toString();
                    requestCargarListaDeHeroes();
                }else{
                    SECONDARY_ROL = "All";
                    requestCargarListaDeHeroes();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // GameLevel
        RadioGroup radioGroup = parentView.findViewById(R.id.gameLevelRadioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.earlyGame:
                        GAME_LEVEL = GAME_LEVEL_EARLY;
                        toolbar.setTitle("Early Game Lvl 1 - 60");
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.midGame:
                        GAME_LEVEL = GAME_LEVEL_MID;
                        toolbar.setTitle("Mid Game Lvl 61 - 160");
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.lateGame:
                        GAME_LEVEL = GAME_LEVEL_LATE;
                        toolbar.setTitle("Late Game Lvl 161 - 240+");
                        requestCargarListaDeHeroes();
                        break;
                }
            }
        });

        // Section
        RadioGroup radioGroupSections = parentView.findViewById(R.id.sectionsRadioGroup);
        radioGroupSections.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.sections_overall:
                        SECTION = OVERALL;
                        requestCargarListaDeHeroes();
                        toolbar.setSubtitle("Overall");

                        break;
                    case R.id.sections_pvp:
                        SECTION = PVP;
                        requestCargarListaDeHeroes();
                        toolbar.setSubtitle("PVP");

                        break;
                    case R.id.sections_pve:
                        SECTION = PVE;
                        requestCargarListaDeHeroes();
                        toolbar.setSubtitle("PVE");

                        break;
                    case R.id.sections_lab:
                        SECTION = LAB;
                        requestCargarListaDeHeroes();
                        toolbar.setSubtitle("LAB");

                        break;
                    case R.id.sections_wrizz:
                        SECTION = WRIZZ;
                        requestCargarListaDeHeroes();
                        toolbar.setSubtitle("WRIZZ");

                        break;
                    case R.id.sections_soren:
                        SECTION = SOREN;
                        requestCargarListaDeHeroes();
                        toolbar.setSubtitle("SOREN");

                        break;
                }
            }
        });

        RadioGroup rarityRadioGroup = parentView.findViewById(R.id.rarityRadioGroup);
        rarityRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rarity_all:
                        RARITY = "All";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.rarity_legendary:
                        RARITY = "Legendary+";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.rarity_ascend:
                        RARITY = "Ascended";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.rarity_common:
                        RARITY = "Common";
                        requestCargarListaDeHeroes();
                        break;
                }
            }
        });


        // Race
        RadioGroup race_nameRadioGroup = parentView.findViewById(R.id.race_nameRadioGroup);
        race_nameRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.race_name_all:
                        RACE_NAME = "All";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.race_name_lightbearer:
                        RACE_NAME = RACE_NAME_LIGHTBEARERS;
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.race_name_mauler:
                        RACE_NAME = RACE_NAME_MAULERS;
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.race_name_wilder:
                        RACE_NAME = RACE_NAME_WILDERS;
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.race_name_graveborn:
                        RACE_NAME = RACE_NAME_GRAVEBORN;
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.race_name_celestial:
                        RACE_NAME = RACE_NAME_CELESTIAL;
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.race_name_hypogean:
                        RACE_NAME = RACE_NAME_HYPOGEAN;
                        requestCargarListaDeHeroes();
                        break;
                }
            }
        });

        // Class
        RadioGroup classeRadioGroup = parentView.findViewById(R.id.classeRadioGroup);
        classeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.classe_all:
                        CLASSE = "All";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.classe_agility:
                        CLASSE = CLASSE_AGILITY;
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.classe_strength:
                        CLASSE = CLASSE_STRENGTH;
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.classe_inteligence:
                        CLASSE = CLASSE_INTELLIGENCE;
                        requestCargarListaDeHeroes();
                        break;
                }
            }
        });


        // Class
        RadioGroup positionRadioGroup = parentView.findViewById(R.id.positionRadioGroup);
        positionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.position_all:
                        POSITION = "All";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.position_any:
                        POSITION = "Any";
;
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.position_back:
                        POSITION = "Back";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.position_front:
                        POSITION = "Front";
                        requestCargarListaDeHeroes();
                        break;
                }
            }
        });

        // Class
        RadioGroup unionRadioGroup = parentView.findViewById(R.id.unionRadioGroup);
        unionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.union_all:
                        UNION = "All";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.union_yes:
                        UNION = "Yes";
                        requestCargarListaDeHeroes();
                        break;
                    case R.id.union_no:
                        UNION = "No";
                        requestCargarListaDeHeroes();
                        break;
                }
            }
        });
    }


    private void requestCargarListaDeHeroes() {
        ListHeroesModel listHeroesModel = new ListHeroesModel();
        listHeroesModel.setSection(SECTION);
        listHeroesModel.setGameLevel(GAME_LEVEL);
        listHeroesModel.setRace_name(RACE_NAME);
        listHeroesModel.setPosition(POSITION);
        listHeroesModel.setClasse(CLASSE);
        listHeroesModel.setUnion(UNION);
        listHeroesModel.setRarity(RARITY);
        listHeroesModel.setPrimary_rol(PRIMARY_ROL);
        listHeroesModel.setSecondary_rol(SECONDARY_ROL);

        DataMaster dataMaster = new DataMaster();
        Data data = new Data();
        data.setList(listHeroesModel);
        dataMaster.setData(data);
            heroViewModel.list_advanced(dataMaster).observe(this, new Observer<Resource<DataMaster>>() {
                @Override
                public void onChanged(Resource<DataMaster> dataMasterResource) {
                    procesaRespuesta(dataMasterResource);
                }
            });
    }


    // JSON
    private String read(Context context, String fileName) {
        try {
            FileInputStream fis = context.openFileInput(fileName);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException fileNotFound) {
            return null;
        } catch (IOException ioException) {
            return null;
        }
    }


    private boolean create(Context context, String fileName, String jsonString){
        String FILENAME = "hero_list.json";
        try {
            FileOutputStream fos = context.openFileOutput(fileName,Context.MODE_PRIVATE);
            if (jsonString != null) {
                fos.write(jsonString.getBytes());
            }
            fos.close();
            return true;
        } catch (FileNotFoundException fileNotFound) {
            return false;
        } catch (IOException ioException) {
            return false;
        }
    }

    public boolean isFilePresent(Context context, String fileName) {
        String path = context.getFilesDir().getAbsolutePath() + "/" + fileName;
        File file = new File(path);
        return file.exists();
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}
