package com.mxlapps.app.afk_arenaguide.Repository;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.UserApi;
import com.mxlapps.app.afk_arenaguide.Service.NetworkBoundResource;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Service.RetrofitRequest;

import retrofit2.Call;

public class UserRepository {

    private UserApi apiService;
    private  static UserRepository instance;
    private Context context;

    public static UserRepository getInstance(Context context){
        if(instance == null){
            instance = new UserRepository(context);
        }
        return instance;
    }
    public UserRepository(Context context) {
        apiService = RetrofitRequest.getInstance().create(UserApi.class);
        this.context = context;
    }

    public LiveData<Resource<DataMaster>> createUser(final DataMaster dataMaster) {

        return new NetworkBoundResource<DataMaster, DataMaster>() {
            @NonNull
            @Override
            protected Call<DataMaster> createCallRetrofit() {
                return apiService.createUser("1234567890",dataMaster);
            }
        }.getAsLiveData();
    }

    public LiveData<Resource<DataMaster>> showUser(final String user_token) {

        return new NetworkBoundResource<DataMaster, DataMaster>() {
            @NonNull
            @Override
            protected Call<DataMaster> createCallRetrofit() {
                return apiService.showUser("1234567890",user_token);
            }
        }.getAsLiveData();
    }

    public LiveData<Resource<DataMaster>> getProfile(final Integer userId) {

        return new NetworkBoundResource<DataMaster, DataMaster>() {
            @NonNull
            @Override
            protected Call<DataMaster> createCallRetrofit() {
                return apiService.getProfile(userId);
            }
        }.getAsLiveData();
    }



    public LiveData<Resource<DataMaster>> updateProfile(final Integer userId, final DataMaster dataMaster) {

        return new NetworkBoundResource<DataMaster, DataMaster>() {
            @NonNull
            @Override
            protected Call<DataMaster> createCallRetrofit() {
                return apiService.updateProfile(userId, dataMaster);
            }
        }.getAsLiveData();
    }


}
