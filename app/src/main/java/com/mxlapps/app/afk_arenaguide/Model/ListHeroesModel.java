package com.mxlapps.app.afk_arenaguide.Model;

public class ListHeroesModel {

    private String gameLevel;
    private String section;
    private String rarity;
    private String classe;
    private String race_name;
    private String position;
    private String synergy;
    private String primary_rol;
    private String secondary_rol;
    private String artifact;
    private String union;
    private String type;

    public ListHeroesModel() {
        this.gameLevel = "All";
        this.section = "All";
        this.rarity = "All";
        this.classe = "All";
        this.race_name = "All";
        this.position = "All";
        this.synergy = "All";
        this.primary_rol = "All";
        this.secondary_rol = "All";
        this.artifact = "All";
        this.union = "All";
        this.type = "All";
    }

    public String getGameLevel() {
        return gameLevel;
    }

    public void setGameLevel(String gameLevel) {
        this.gameLevel = gameLevel;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getRace_name() {
        return race_name;
    }

    public void setRace_name(String race_name) {
        this.race_name = race_name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSynergy() {
        return synergy;
    }

    public void setSynergy(String synergy) {
        this.synergy = synergy;
    }

    public String getPrimary_rol() {
        return primary_rol;
    }

    public void setPrimary_rol(String primary_rol) {
        this.primary_rol = primary_rol;
    }

    public String getSecondary_rol() {
        return secondary_rol;
    }

    public void setSecondary_rol(String secondary_rol) {
        this.secondary_rol = secondary_rol;
    }

    public String getArtifact() {
        return artifact;
    }

    public void setArtifact(String artifact) {
        this.artifact = artifact;
    }

    public String getUnion() {
        return union;
    }

    public void setUnion(String union) {
        this.union = union;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
