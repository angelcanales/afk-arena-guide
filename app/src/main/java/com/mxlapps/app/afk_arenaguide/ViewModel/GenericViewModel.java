package com.mxlapps.app.afk_arenaguide.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mxlapps.app.afk_arenaguide.Model.StrengthWeaknessModel;
import com.mxlapps.app.afk_arenaguide.Repository.GenericRepository;
import com.mxlapps.app.afk_arenaguide.Request.Data;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;

public class GenericViewModel extends AndroidViewModel {

    private GenericRepository repository;

    public GenericViewModel(@NonNull Application application) {
        super(application);
        repository = GenericRepository.getInstance(application);
    }


    public LiveData<Resource<DataMaster>> createStrengthWeakness(StrengthWeaknessModel strengthWeaknessModel) {
        Data data = new Data();
        data.setStrengthWeakness(strengthWeaknessModel);
        DataMaster dataMaster = new DataMaster();
        dataMaster.setData(data);
        return repository.createStrengthWeakness(dataMaster);
    }


}
