package com.mxlapps.app.afk_arenaguide.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.ItemsModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>{

    private ArrayList<ItemsModel> items;

    public ItemsAdapter(ArrayList<ItemsModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_items_section, parent, false);
        return new ItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemsViewHolder holder, int position) {
        final ItemsModel item = items.get(position);
        holder.textView_item_title.setText(item.getItem_title());
        holder.textView_item_desc.setText(item.getDesc());
        Picasso.get().load(item.getItem_image()).into(holder.imageView_item_image);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ItemsViewHolder extends RecyclerView.ViewHolder {

        TextView textView_item_desc;
        TextView textView_item_title;
        ImageView imageView_item_image;

        ItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_item_title    = itemView.findViewById(R.id.textView_item_title);
            textView_item_desc    = itemView.findViewById(R.id.textView_item_desc);
            imageView_item_image = itemView.findViewById(R.id.imageView_item_image);
        }
    }
}
