package com.mxlapps.app.afk_arenaguide.Views.Heroes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.mxlapps.app.afk_arenaguide.Adapter.HeroDetailAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.HeroViewModel;
import com.squareup.picasso.Picasso;


public class HeroDetailV2 extends AppCompatActivity {

    private static final String TAG = HeroDetailV2.class.getName();
    HeroViewModel heroViewModel;
    View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero_detailv2);
        Util.setContext(this);

        // RootView
        rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        heroViewModel = ViewModelProviders.of(HeroDetailV2.this).get(HeroViewModel.class);


        
        Intent intent = getIntent();
        int hero_id = intent.getIntExtra("hero_id", 0);
        heroViewModel.getHeroDetail(hero_id).observe(HeroDetailV2.this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource);
            }
        });


        Toolbar toolbar = findViewById(R.id.toolbar_detalle_heroe);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


//        Util.initAds(rootView, this, BuildConfig.AD_DETAIL);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_OK);
                finish();
                return true;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    private void heroDetail(Integer hero_id) {
        heroViewModel.getHeroDetail(hero_id).observe(HeroDetailV2.this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource);
            }
        });
    }



    private void procesaRespuesta(Resource<DataMaster> dataMasterResource){
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status){
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, HeroDetailV2.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                // Se obtiene la informacion del heroe
                Log.d(TAG, "procesaRespuesta: Fue success");
                responseHeroDetail(dataMasterResource);

                break;
            default:
                break;
        }
    }

    private void responseHeroDetail(Resource<DataMaster> dataMasterResource) {
        // saca el drawable
        if (dataMasterResource.data != null){
            HeroModel hero = dataMasterResource.data.getData().getHero();

            // Seteta nombre, groupname e imagen
            ImageView imageView_hero_icon = findViewById(R.id.imageView_hero_icon);
            Picasso.get().load(hero.getSmallImage()).into(imageView_hero_icon);

            TextView textView_hero_name = findViewById(R.id.textView_hero_name);
            textView_hero_name.setText(hero.getName());
            TextView textView_group_name = findViewById(R.id.textView_group_name);
            textView_group_name.setText(hero.getGroup());

            ViewPager viewPager = findViewById(R.id.pager);
            HeroDetailAdapter heroDetailAdapter = new HeroDetailAdapter(getSupportFragmentManager(), hero);
            viewPager.setOffscreenPageLimit(6);

            viewPager.setAdapter(heroDetailAdapter);
            TabLayout tabLayout = findViewById(R.id.tablayout);
            tabLayout.setupWithViewPager(viewPager);
            TabLayout.Tab tab = tabLayout.getTabAt(1);
            tab.select();



        }
    }



}
