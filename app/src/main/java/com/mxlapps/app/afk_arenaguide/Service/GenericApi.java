package com.mxlapps.app.afk_arenaguide.Service;

import com.mxlapps.app.afk_arenaguide.Request.DataMaster;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GenericApi {
    String STRENGTH_WEAKNESS = "extras/create_StrengthWeakness";


    @GET(STRENGTH_WEAKNESS)
    Call<DataMaster> getContributors(@Query("token") String user_token);


    @POST(STRENGTH_WEAKNESS)
    Call<DataMaster> createStrengthWeakness(@Body DataMaster dataMaster);

}
