package com.mxlapps.app.afk_arenaguide.Views.Sections;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.mxlapps.app.afk_arenaguide.Adapter.FaqAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.FaqModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.ExtraViewModel;

import java.util.ArrayList;

public class FaqActivity extends AppCompatActivity {
    private ExtraViewModel extraViewModel;
    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_global);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("AFK GUIDE - FAQ");
            getSupportActionBar().setSubtitle("Frequent Ask Questions");
        }

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        extraViewModel = ViewModelProviders.of(FaqActivity.this).get(ExtraViewModel.class);
        fillInfo();

//        Util.initAds(rootView, this, BuildConfig.ADS_FAQ);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void fillInfo() {
        extraViewModel.getFaq().observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });
    }

    private void populateRecycler(ArrayList<FaqModel> faqModels) {
        RecyclerView recyclerView = findViewById(R.id.recycler_global);
        LinearLayoutManager layoutManager = new LinearLayoutManager(FaqActivity.this);
        FaqAdapter adapter = new FaqAdapter(faqModels);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion) {
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, FaqActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);

                assert dataMasterResource.data != null;
                ArrayList<FaqModel> faq = dataMasterResource.data.getData().getFaq();
                populateRecycler(faq);
                break;
            default:
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        setResult(RESULT_OK);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
