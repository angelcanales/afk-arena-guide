package com.mxlapps.app.afk_arenaguide.Views.Decks;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.mxlapps.app.afk_arenaguide.Adapter.HeroesDeckAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.AppPreferences;
import com.mxlapps.app.afk_arenaguide.Utils.CustomDialogClass;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.HeroViewModel;
import com.mxlapps.app.afk_arenaguide.Views.LoginFacebookActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CreateDeckActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "afkArenaMainActivity";
    private ArrayList<HeroModel> heroModels;
    private ArrayList<HeroModel> heroSeleccionados;
    private Double cost;
    private int pinHolderSelected = 0;
    private ArrayList<Integer> pinIds = new ArrayList<>();
    private View rootView;
    View v;
    private HeroViewModel heroViewModel;
    TextView textView_save_deck;
    TextView textView_cost;

    ImageView imageView_info_build_team;

    ImageView imageViewDeck1;
    ImageView imageViewDeck2;
    ImageView imageViewDeck3;
    ImageView imageViewDeck4;
    ImageView imageViewDeck5;

    ImageView imageView_selectAll;
    ImageView imageView_i_human;
    ImageView imageView_i_graveborn;
    ImageView imageView_i_hypogean;
    ImageView imageView_i_celestial;
    ImageView imageView_i_wild;
    ImageView imageView_i_orc;


    String deck1_id = "";
    String deck2_id = "";
    String deck3_id = "";
    String deck4_id = "";
    String deck5_id = "";

    Double costo1 = 0.0;
    Double costo2 = 0.0;
    Double costo3 = 0.0;
    Double costo4 = 0.0;
    Double costo5 = 0.0;

    private DeckModel deckModel = new DeckModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_save_deck);

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        // ViewModels
        heroViewModel = ViewModelProviders.of(CreateDeckActivity.this).get(HeroViewModel.class);

        requestCargarListaDeHeroes();
        agreaListenrClickHolders();


//        MobileAds.initialize(this, BuildConfig.ADS_DECK_CREATE);
//        AdView mAdView = findViewById(R.id.adView_create_deck);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        buildteamTutorial();
    }

    private void buildteamTutorial() {
        Boolean tutorialDone = AppPreferences.getInstance(CreateDeckActivity.this).getAlertBuildTeamsTutorial();

        if (!tutorialDone) {
            CustomDialogClass cdd = new CustomDialogClass(CreateDeckActivity.this);
            cdd.show();
            AppPreferences.getInstance(CreateDeckActivity.this).setAlertBuildTeamsTutorial(true);
        }

    }


    private void agreaListenrClickHolders() {
        /// Filtros
        imageView_info_build_team = findViewById(R.id.imageView_info_build_team);



        imageView_selectAll = findViewById(R.id.imageView_selectAll);
        imageView_selectAll.setOnClickListener(this);
        imageView_i_human = findViewById(R.id.imageView_i_human);
        imageView_i_human.setOnClickListener(this);
        imageView_i_orc = findViewById(R.id.imageView_i_orc);
        imageView_i_orc.setOnClickListener(this);
        imageView_i_graveborn = findViewById(R.id.imageView_i_graveborn);
        imageView_i_graveborn.setOnClickListener(this);
        imageView_i_hypogean = findViewById(R.id.imageView_i_hypogean);
        imageView_i_hypogean.setOnClickListener(this);
        imageView_i_celestial = findViewById(R.id.imageView_i_celestial);
        imageView_i_celestial.setOnClickListener(this);
        imageView_i_wild = findViewById(R.id.imageView_i_wild);
        imageView_i_wild.setOnClickListener(this);

        imageViewDeck1 = findViewById(R.id.imageViewDeck1);
        imageViewDeck1.setOnClickListener(this);

        imageViewDeck2 = findViewById(R.id.imageViewDeck2);
        imageViewDeck2.setOnClickListener(this);

        imageViewDeck3 = findViewById(R.id.imageViewDeck3);
        imageViewDeck3.setOnClickListener(this);

        imageViewDeck4 = findViewById(R.id.imageViewDeck4);
        imageViewDeck4.setOnClickListener(this);

        imageViewDeck5 = findViewById(R.id.imageViewDeck5);
        imageViewDeck5.setOnClickListener(this);

        textView_save_deck = findViewById(R.id.textView_save_deck);
        textView_save_deck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String userId = AppPreferences.getInstance(CreateDeckActivity.this).getUserToken();
                // Valida que el usuario este logeado
                if (userId.compareToIgnoreCase("") == 0) {

                    Intent intent = new Intent(CreateDeckActivity.this, LoginFacebookActivity.class);
                    startActivityForResult(intent, 1001);

                }else{

                    // Valida si hay pins repetidos
                    deckModel.setUser_id(String.valueOf(AppPreferences.getInstance(CreateDeckActivity.this).getUsuario_id()));
                    deckModel.setAuthor(AppPreferences.getInstance(CreateDeckActivity.this).getEmail());
                    deckModel.setHero1(deck1_id);
                    deckModel.setHero2(deck2_id);
                    deckModel.setHero3(deck3_id);
                    deckModel.setHero4(deck4_id);
                    deckModel.setHero5(deck5_id);

                    if ((deck1_id.compareToIgnoreCase("") == 0) ||(deck2_id.compareToIgnoreCase("") == 0) ||
                            (deck3_id.compareToIgnoreCase("") == 0) ||(deck4_id.compareToIgnoreCase("") == 0) ||
                            (deck5_id.compareToIgnoreCase("") == 0) ){
                        Toast.makeText(CreateDeckActivity.this, "Select 5 heroes", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (buscaRepetidos()){
                        Toast.makeText(CreateDeckActivity.this, "You can't repeat heroes", Toast.LENGTH_SHORT).show();
                    }else{
                        DeckSaveNewDeckFragment deckSaveNewDeckFragment = new DeckSaveNewDeckFragment(deckModel);
                        deckSaveNewDeckFragment.SetOnItemClickListener(new DeckSaveNewDeckFragment.CerrarTodo() {
                            @Override
                            public void cierrate() {
                                finish();
                            }
                        });
                        deckSaveNewDeckFragment.show(CreateDeckActivity.this.getSupportFragmentManager(),"deckSaveNewDeckFragment");
                    }
                }
            }
        });


        imageView_info_build_team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogClass cdd = new CustomDialogClass(CreateDeckActivity.this);
                cdd.show();
            }
        });
    }

    private void initRecyclerView(final ArrayList<HeroModel> pinModelsInternal) {
        RecyclerView recyclerView = findViewById(R.id.recycler_pinListDeck);
        int numberOfColumns = 5;
        HeroesDeckAdapter adapter = new HeroesDeckAdapter(pinModelsInternal, CreateDeckActivity.this);
        recyclerView.setLayoutManager(new GridLayoutManager(CreateDeckActivity.this, numberOfColumns));

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        final int color = Color.parseColor("#00000000");
        adapter.SetOnItemClickListener(new HeroesDeckAdapter.OnItemClickListener() {
            @Override
            public void onHeroCardClick(int position) {
                Log.d(TAG, "onHeroCardClick: " + pinIds.toString());
                // Valida si ya fue agregado el pin
                if (pinHolderSelected != 0){
                    String imgLink = pinModelsInternal.get(position).getSmallImage();
                    switch (pinHolderSelected){
                        case 1:
                            Picasso.get().load(imgLink).into(imageViewDeck1);
                            deck1_id = pinModelsInternal.get(position).getName();
                            break;
                        case 2:
                            Picasso.get().load(imgLink).into(imageViewDeck2);
                            deck2_id = pinModelsInternal.get(position).getName();

                            break;
                        case 3:
                            Picasso.get().load(imgLink).into(imageViewDeck3);
                            deck3_id = pinModelsInternal.get(position).getName();

                            break;
                        case 4:
                            Picasso.get().load(imgLink).into(imageViewDeck4);
                            deck4_id = pinModelsInternal.get(position).getName();

                            break;
                        case 5:
                            Picasso.get().load(imgLink).into(imageViewDeck5);
                            deck5_id = pinModelsInternal.get(position).getName();

                            break;

                    }
                    pinHolderSelected = 0;
                }else{
                    Toast.makeText(CreateDeckActivity.this, "Please select a holder", Toast.LENGTH_SHORT).show();
                    pinHolderSelected = 0;
                }


                imageViewDeck1.setBackgroundColor(color);
                imageViewDeck2.setBackgroundColor(color);
                imageViewDeck3.setBackgroundColor(color);
                imageViewDeck4.setBackgroundColor(color);
                imageViewDeck5.setBackgroundColor(color);
            }

        });

    }

    public Boolean buscaRepetidos(){
        ArrayList<String> pinsSeleccionados = new ArrayList<>();
        pinsSeleccionados.add(deck1_id);
        pinsSeleccionados.add(deck2_id);
        pinsSeleccionados.add(deck3_id);
        pinsSeleccionados.add(deck4_id);
        pinsSeleccionados.add(deck5_id);

        for (String pinName : pinsSeleccionados) {

            int cont = 0;
            for (int y = 0; y < pinsSeleccionados.size(); y++){
                if (pinName.compareToIgnoreCase(pinsSeleccionados.get(y)) == 0) {
                    if (pinName.compareToIgnoreCase("") != 0) {
                        cont++;
                    }
                }
                if (cont == 2){
                    return true;
                }
            }
            cont = 0;
        }

        return false;
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion) {
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, CreateDeckActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                // Crea copia de listado de heroes
                switch (opcion) {
                    case 1:
                        heroModels = dataMasterResource.data.getData().getHeroModels();
                        initListadoHeroes();
                }
                break;
            default:
                break;
        }
    }

    private void initListadoHeroes() {
        initRecyclerView(heroModels);
    }

    private void requestCargarListaDeHeroes() {
        heroViewModel.getHeroList("tier_list_earlies", "overall", "All", "All", "All").observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int color = Color.parseColor("#5e43c9");
        switch (view.getId()){
            case R.id.imageViewDeck1:
                imageViewDeck1.setBackgroundColor(color);
                pinHolderSelected = 1;
                break;
            case R.id.imageViewDeck2:
                imageViewDeck2.setBackgroundColor(color);
                pinHolderSelected = 2;
                break;
            case R.id.imageViewDeck3:
                imageViewDeck3.setBackgroundColor(color);
                pinHolderSelected = 3;
                break;
            case R.id.imageViewDeck4:
                imageViewDeck4.setBackgroundColor(color);
                pinHolderSelected = 4;
                break;
            case R.id.imageViewDeck5:
                imageViewDeck5.setBackgroundColor(color);
                pinHolderSelected = 5;
                break;

            case R.id.imageView_selectAll:
                filtraRecyclerView("All");
                break;
            case R.id.imageView_i_human:
                filtraRecyclerView("Lightbearer");
                break;
            case R.id.imageView_i_graveborn:
                filtraRecyclerView("Graveborn");
                break;
            case R.id.imageView_i_hypogean:
                filtraRecyclerView("Hypogean");
                break;
            case R.id.imageView_i_celestial:
                filtraRecyclerView("Celestial");
                break;
            case R.id.imageView_i_wild:
                filtraRecyclerView("Wilder");
                break;
            case R.id.imageView_i_orc:
                filtraRecyclerView("Mauler");
                break;
        }
    }

    private void filtraRecyclerView(String race) {

        heroSeleccionados = new ArrayList<HeroModel>();

        if (race.compareToIgnoreCase("All") == 0){
            heroSeleccionados.addAll(heroModels);
        }else{

            for (int x = 0; x < heroModels.size(); x++){

                if (heroModels.get(x).getRace_name().compareToIgnoreCase(race) == 0){
                    heroSeleccionados.add(heroModels.get(x));
                }
            }
        }
        initRecyclerView(heroSeleccionados);
    }

    @Override
    public void finish() {
        setResult(RESULT_OK);
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
