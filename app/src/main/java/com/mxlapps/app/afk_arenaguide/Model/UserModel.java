package com.mxlapps.app.afk_arenaguide.Model;

public class UserModel {

    private Integer id;
    private String token;
    private String facebook_name;
    private String game_name;
    private String name;
    private String email;
    private String fb_image;

    public String getFb_image() {
        return fb_image;
    }

    public void setFb_image(String fb_image) {
        this.fb_image = fb_image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFacebook_name() {
        return facebook_name;
    }

    public void setFacebook_name(String facebook_name) {
        this.facebook_name = facebook_name;
    }

    public String getGame_name() {
        return game_name;
    }

    public void setGame_name(String game_name) {
        this.game_name = game_name;
    }
}
