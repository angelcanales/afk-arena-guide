package com.mxlapps.app.afk_arenaguide.Model;


public class VoteModel {
    private Integer user;
    private String section;
    private Float vote;
    private Integer item_id;

    public Integer getUser_id() {
        return user;
    }

    public void setUser_id(Integer user) {
        this.user = user;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Float getVote() {
        return vote;
    }

    public void setVote(Float vote) {
        this.vote = vote;
    }

    public Integer getItem_id() {
        return item_id;
    }

    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }
}
