package com.mxlapps.app.afk_arenaguide.Views.Sections;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.mxlapps.app.afk_arenaguide.Adapter.ItemsAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.ItemsModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.ExtraViewModel;

import java.util.ArrayList;

public class ItemsActivity extends AppCompatActivity {

    private ExtraViewModel extraViewModel;
    private View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_global);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("AFK GUIDE - ITEMS");
            getSupportActionBar().setSubtitle("Items information");
        }

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        extraViewModel = ViewModelProviders.of(ItemsActivity.this).get(ExtraViewModel.class);
        fillInfo();

//        Util.initAds(rootView, this, BuildConfig.ADS_ITEMS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    private void fillInfo() {
        extraViewModel.getItems().observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });
    }


    private void populateRecycler(ArrayList<ItemsModel> itemsModel) {
        RecyclerView recyclerView = findViewById(R.id.recycler_global);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ItemsActivity.this);
        ItemsAdapter adapter = new ItemsAdapter(itemsModel);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion) {
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, ItemsActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);

                assert dataMasterResource.data != null;
                ArrayList<ItemsModel> itemsArray = dataMasterResource.data.getData().getItems();
                populateRecycler(itemsArray);
                break;
            default:
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        setResult(RESULT_OK);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
