package com.mxlapps.app.afk_arenaguide.Views.Sections;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Utils.AppPreferences;

public class SettingsActivity extends AppCompatActivity {

    RadioGroup radioGroup_language;
    RadioButton radioButton_en;
    RadioButton radioButton_ru;
    RadioButton radioButton_ge;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initViews();
        getLanguage();

    }

    private void getLanguage() {
        // Verifica si ya hay un idioma seleccionado
        String language = AppPreferences.getInstance(this).getLanguage();

        if (language.compareToIgnoreCase("") == 0){
            // No hay lenguaje seleccionado, se setetea "en"
            AppPreferences.getInstance(this).setLanguage("en");
        }

        // Setea el idioma seleccionado
        switch (language){
            case "en":
                radioButton_en.setChecked(true);
                break;
            case "ru":
                radioButton_ru.setChecked(true);
                break;
            case "ge":
                radioButton_ge.setChecked(true);
                break;
        }

        radioGroup_language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId){
                    case R.id.radioButton_en:
                        AppPreferences.getInstance(SettingsActivity.this).setLanguage("en");
                        break;
                    case R.id.radioButton_ru:
                        AppPreferences.getInstance(SettingsActivity.this).setLanguage("ru");
                        break;
                    case R.id.radioButton_ge:
                        AppPreferences.getInstance(SettingsActivity.this).setLanguage("ge");
                        break;
                }
            }
        });
    }


    public void initViews(){
        radioGroup_language = findViewById(R.id.radioGroup_language);
        radioButton_en      = findViewById(R.id.radioButton_en);
        radioButton_ru      = findViewById(R.id.radioButton_ru);
        radioButton_ge      = findViewById(R.id.radioButton_ge);

    }
}
