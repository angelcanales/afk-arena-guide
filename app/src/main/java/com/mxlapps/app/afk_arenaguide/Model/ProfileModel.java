package com.mxlapps.app.afk_arenaguide.Model;

public class ProfileModel {
    private UserModel user;
    private DeckModel deck;

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public DeckModel getDeck() {
        return deck;
    }

    public void setDeck(DeckModel deck) {
        this.deck = deck;
    }
}
