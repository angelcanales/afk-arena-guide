package com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;

public class TierDataFragment extends Fragment {
    View v;
    HeroModel hero;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String heroString = getArguments().getString("hero");
        Gson gson = new Gson();
        this.hero =  gson.fromJson(heroString, HeroModel.class);


        v = inflater.inflate(R.layout.fragment_tierdata, container, false);

        fillInfo();

        return v;
    }

    private void fillInfo() {

        // Early Game
        TextView hero_overall_e = v.findViewById(R.id.hero_overall_e);
        TextView hero_pvp_e     = v.findViewById(R.id.hero_pvp_e);
        TextView hero_pve_e     = v.findViewById(R.id.hero_pve_e);
        TextView hero_lab_e     = v.findViewById(R.id.hero_lab_e);
        TextView hero_wrizz_e   = v.findViewById(R.id.hero_wrizz_e);
        TextView hero_soren_e   = v.findViewById(R.id.hero_soren_e);

        hero_overall_e.setText(Html.fromHtml(hero.getEarly().getOverall()));
        hero_pvp_e.setText(Html.fromHtml(hero.getEarly().getPvp()));
        hero_pve_e.setText(Html.fromHtml(hero.getEarly().getPve()));
        hero_lab_e.setText(Html.fromHtml(hero.getEarly().getLab()));
        hero_wrizz_e.setText(Html.fromHtml(hero.getEarly().getWrizz()));
        hero_soren_e.setText(Html.fromHtml(hero.getEarly().getSoren()));

        // Mid Game
        TextView hero_overall_m = v.findViewById(R.id.hero_overall_m);
        TextView hero_pvp_m     = v.findViewById(R.id.hero_pvp_m);
        TextView hero_pve_m     = v.findViewById(R.id.hero_pve_m);
        TextView hero_lab_m     = v.findViewById(R.id.hero_lab_m);
        TextView hero_wrizz_m   = v.findViewById(R.id.hero_wrizz_m);
        TextView hero_soren_m   = v.findViewById(R.id.hero_soren_m);

        hero_overall_m.setText(Html.fromHtml(hero.getMid().getOverall()));
        hero_pvp_m.setText(Html.fromHtml(hero.getMid().getPvp()));
        hero_pve_m.setText(Html.fromHtml(hero.getMid().getPve()));
        hero_lab_m.setText(Html.fromHtml(hero.getMid().getLab()));
        hero_wrizz_m.setText(Html.fromHtml(hero.getMid().getWrizz()));
        hero_soren_m.setText(Html.fromHtml(hero.getMid().getSoren()));
        Log.d("fillInfo", "fillInfo: " + hero.getLate().toString());
        if (hero.getLate().getOverall().compareToIgnoreCase("") != 0){
            // Late Game
            TextView hero_overall_l = v.findViewById(R.id.hero_overall_l);
            TextView hero_pvp_l     = v.findViewById(R.id.hero_pvp_l);
            TextView hero_pve_l     = v.findViewById(R.id.hero_pve_l);
            TextView hero_lab_l     = v.findViewById(R.id.hero_lab_l);
            TextView hero_wrizz_l   = v.findViewById(R.id.hero_wrizz_l);
            TextView hero_soren_l   = v.findViewById(R.id.hero_soren_l);

            hero_overall_l.setText(Html.fromHtml(hero.getLate().getOverall()));
            hero_pvp_l.setText(Html.fromHtml(hero.getLate().getPvp()));
            hero_pve_l.setText(Html.fromHtml(hero.getLate().getPve()));
            hero_lab_l.setText(Html.fromHtml(hero.getLate().getLab()));
            hero_wrizz_l.setText(Html.fromHtml(hero.getLate().getWrizz()));
            hero_soren_l.setText(Html.fromHtml(hero.getLate().getSoren()));
        }else{
            v.findViewById(R.id.constrainLayout_late).setVisibility(View.GONE);
        }



    }


}
