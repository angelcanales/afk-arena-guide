package com.mxlapps.app.afk_arenaguide.Views.Sections;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputEditText;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.Model.ListHeroesModel;
import com.mxlapps.app.afk_arenaguide.Model.ProfileModel;
import com.mxlapps.app.afk_arenaguide.Model.UserModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.Data;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.AppPreferences;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.DecksViewModel;
import com.mxlapps.app.afk_arenaguide.ViewModel.HeroViewModel;
import com.mxlapps.app.afk_arenaguide.ViewModel.UserViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "profileactividaddddd";
    DeckModel deckModel = new DeckModel();
    ImageView imageView_profile_picture;
    TextInputEditText name;
    TextInputEditText email;
    TextInputEditText deckname;
    TextInputEditText desc;
    TextInputEditText strong;
    TextInputEditText weak;
    CheckBox gamelevelEarly;
    CheckBox gamelevelMid;
    CheckBox gamelevelLate;
    CheckBox sectionOverall;
    CheckBox sectionPVP;
    CheckBox sectionPVE;
    CheckBox sectionLAB;
    CheckBox sectionSoren;
    CheckBox sectionWrizz;
    private HeroViewModel heroViewModel;
    private UserViewModel userViewModel;
    private DecksViewModel decksViewModel;
    private ArrayList<HeroModel> heroModels;
    private View rootView;

    ImageView hero1;
    ImageView hero2;
    ImageView hero3;
    ImageView hero4;
    ImageView hero5;

    Integer holderActive = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_profile);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("AFK GUIDE - PROFILE");
        }

        heroViewModel = ViewModelProviders.of(ProfileActivity.this).get(HeroViewModel.class);
        decksViewModel = ViewModelProviders.of(ProfileActivity.this).get(DecksViewModel.class);
        userViewModel = ViewModelProviders.of(ProfileActivity.this).get(UserViewModel.class);
        rootView = getWindow().getDecorView().findViewById(android.R.id.content);

//        Util.initAds(rootView, this, BuildConfig.ADS_PROFILE);

        initiViews();
        requestHeroesList();

    }

    private void requestHeroesList() {
        ListHeroesModel listHeroesModel = new ListHeroesModel();
        listHeroesModel.setSection("overall");
        listHeroesModel.setGameLevel("tier_list_earlies");
        listHeroesModel.setRace_name("All");
        listHeroesModel.setPosition("All");
        listHeroesModel.setClasse("All");
        listHeroesModel.setUnion("All");
        listHeroesModel.setRarity("All");

        DataMaster dataMaster = new DataMaster();
        Data data = new Data();
        data.setList(listHeroesModel);
        dataMaster.setData(data);

        heroViewModel.list_advanced(dataMaster).observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 2);
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profile_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
            case R.id.menu_refresh:
                Log.d(TAG, "onOptionsItemSelected: se trata de actualizar el perfil");
                updateProfile();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }



    public void requestUserProfile(){
        // Hace request para obtener la informacion basica y del deck si esta disponible
        Integer userId = AppPreferences.getInstance(this).getUsuario_id();
        userViewModel.getProfile(userId).observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });
    }

    public Boolean buscaRepetidos(DeckModel deck){
        ArrayList<String> heroesSeleccionados = new ArrayList<>();
        if (deck.getHero1() != null)
            heroesSeleccionados.add(deck.getHero1());
        if (deck.getHero2() != null)
            heroesSeleccionados.add(deck.getHero2());
        if (deck.getHero3() != null)
            heroesSeleccionados.add(deck.getHero3());
        if (deck.getHero4() != null)
            heroesSeleccionados.add(deck.getHero4());
        if (deck.getHero5() != null)
            heroesSeleccionados.add(deck.getHero5());

        for (String hero : heroesSeleccionados) {

            int cont = 0;
            for (int y = 0; y < heroesSeleccionados.size(); y++){
                if (hero != null){
                    if (hero.compareToIgnoreCase(heroesSeleccionados.get(y)) == 0) {
                        if (hero.compareToIgnoreCase("") != 0) {
                            cont++;
                        }
                    }
                    if (cont == 2){
                        return true;
                    }
                }
            }
            cont = 0;
        }

        return false;
    }

    private void updateProfile() {

        // recolecta datos
        Data data = new Data();
        ProfileModel profile;
        profile = recolectaData();
        profile.setDeck(profile.getDeck());
        profile.setUser(profile.getUser());

        // valida que no haya obligatorios vacios

        if (profile.getUser().getName() == null || profile.getUser().getName().compareToIgnoreCase("") == 0){
            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
        }else if(buscaRepetidos(profile.getDeck())){
            // Valida que no haya heroes duplicados
            Toast.makeText(this, "You can't repeat heroes", Toast.LENGTH_SHORT).show();
        }else{

            // si esta bien, envia request
            data.setProfile(profile);
            DataMaster dataMaster = new DataMaster();
            dataMaster.setData(data);

            Integer userID = AppPreferences.getInstance(this).getUsuario_id();

            userViewModel.updateProfile(userID, dataMaster).observe(this, new Observer<Resource<DataMaster>>() {
                @Override
                public void onChanged(Resource<DataMaster> dataMasterResource) {
                    procesaRespuesta(dataMasterResource, 3);
                }
            });
        }
    }

    private ProfileModel recolectaData() {

        ProfileModel profileModel = new ProfileModel();

        DeckModel deck = new DeckModel();

        String deckName = (deckname.getText() != null) ? deckname.getText().toString(): "";
        String deckDesc = (desc.getText() != null) ? desc.getText().toString(): "";
        String deckStr= (strong.getText() != null) ? strong.getText().toString(): "";
        String deckWeak= (weak.getText() != null) ? weak.getText().toString(): "";

        boolean gle = gamelevelEarly.isChecked();
        boolean glm = gamelevelMid.isChecked();
        boolean gll = gamelevelLate.isChecked();

        boolean ov = sectionOverall.isChecked();
        boolean pvp = sectionPVP.isChecked();
        boolean pve = sectionPVE.isChecked();
        boolean lab = sectionLAB.isChecked();
        boolean wr = sectionWrizz.isChecked();
        boolean so = sectionSoren.isChecked();


        deck.setName(deckName);
        deck.setDesc(deckDesc);
        deck.setStrongvs(deckStr);
        deck.setWeakvs(deckWeak);
        deck.setGame_level_early((gle)? 1:0);
        deck.setGame_level_mid((glm)? 1:0);
        deck.setGame_level_late((gll)? 1:0);

        deck.setSection_overall((ov)? 1:0);
        deck.setSection_pvp((pvp)? 1:0);
        deck.setSection_pve((pve)? 1:0);
        deck.setSection_lab((lab)? 1:0);
        deck.setSection_wrizz((wr)? 1:0);
        deck.setSection_soren((so)? 1:0);

        deck.setHero1(deckModel.getHero1());
        deck.setHero2(deckModel.getHero2());
        deck.setHero3(deckModel.getHero3());
        deck.setHero4(deckModel.getHero4());
        deck.setHero5(deckModel.getHero5());


        String newName = (name.getText() != null) ? name.getText().toString():"";

        UserModel userModel = new UserModel();
        userModel.setName(newName);

        profileModel.setDeck(deck);
        profileModel.setUser(userModel);

        return profileModel;
    }


    private void initiViews() {
        imageView_profile_picture = findViewById(R.id.imageView_profile_picture);
        name = findViewById(R.id.edittext_name);
        email = findViewById(R.id.edittext_email);

        deckname = findViewById(R.id.deckName);
        desc = findViewById(R.id.deckDesc);
        strong = findViewById(R.id.deckStrengt);
        weak = findViewById(R.id.deckWeakt);
        gamelevelEarly = findViewById(R.id.checkbox_Early);
        gamelevelMid = findViewById(R.id.checkbox_Mid);
        gamelevelLate = findViewById(R.id.checkbox_Late);
        sectionOverall = findViewById(R.id.checkbox_Overall);
        sectionPVP = findViewById(R.id.checkbox_PVP);
        sectionPVE = findViewById(R.id.checkbox_PVE);
        sectionLAB = findViewById(R.id.checkbox_LAB);
        sectionSoren = findViewById(R.id.checkbox_Soren);
        sectionWrizz = findViewById(R.id.checkbox_Wrizz);

        hero1 = findViewById(R.id.imageView_hero1);
        hero2 = findViewById(R.id.imageView_hero2);
        hero3 = findViewById(R.id.imageView_hero3);
        hero4 = findViewById(R.id.imageView_hero4);
        hero5 = findViewById(R.id.imageView_hero5);

        hero1.setOnClickListener(this);
        hero2.setOnClickListener(this);
        hero3.setOnClickListener(this);
        hero4.setOnClickListener(this);
        hero5.setOnClickListener(this);

    }

    public String getImageUrl(String heroName){
        for (HeroModel h: heroModels ) {
            if (h.getName() != null && heroName != null){
                if (heroName.compareToIgnoreCase(h.getName()) == 0){
                    return h.getSmallImage();
                }
            }
        }
        return "";
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opc) {
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, ProfileActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                switch (opc){
                    case 1:
                        if (dataMasterResource.data.getData().isError()){
                            if (dataMasterResource.data.getData().getMsg().compareToIgnoreCase("Can't find that User.") == 0){
                                Toast.makeText(this, "Please login again", Toast.LENGTH_SHORT).show();
                                AppPreferences.cerrarSesion();
                            }else{
                                Toast.makeText(this, dataMasterResource.data.getData().getMsg(), Toast.LENGTH_SHORT).show();
                            }

                            finish();
                        }else{
                            ProfileModel profile = dataMasterResource.data.getData().getProfile();
                            loadProfileData(profile);
                        }
                        break;
                    case 2:
                        // Crea copia de listado de heroes y queda lista para pasarlo al bottomsheet
                        heroModels = dataMasterResource.data.getData().getHeroModels();
                        requestUserProfile();
                        break;
                    case 3:
                        Toast.makeText(this, "Your profile has been updated!", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void loadProfileData(ProfileModel profile) {

        UserModel user = profile.getUser();

        String token = AppPreferences.getInstance(this).getUserToken();

        Log.d(TAG, "loadProfileData: " + token);
        Picasso.get().load(user.getFb_image()).into(imageView_profile_picture);
        name.setText(user.getName());
        email.setText(user.getEmail());

        DeckModel deck = profile.getDeck();

        if (deck.getHero1() != null && deck.getHero1().compareToIgnoreCase("") != 0){
            Picasso.get().load(getImageUrl(deck.getHero1())).into(hero1);
            deckModel.setHero1(deck.getHero1());
        }

        if (deck.getHero2() != null && deck.getHero2().compareToIgnoreCase("") != 0){
            Picasso.get().load(getImageUrl(deck.getHero2())).into(hero2);
            deckModel.setHero2(deck.getHero2());
        }
        if (deck.getHero3() != null && deck.getHero3().compareToIgnoreCase("") != 0){
            Picasso.get().load(getImageUrl(deck.getHero3())).into(hero3);
            deckModel.setHero3(deck.getHero3());
        }

        if (deck.getHero4() != null && deck.getHero4().compareToIgnoreCase("") != 0){
            Picasso.get().load(getImageUrl(deck.getHero4())).into(hero4);
            deckModel.setHero4(deck.getHero4());
        }
        if (deck.getHero5() != null && deck.getHero5().compareToIgnoreCase("") != 0){
            Picasso.get().load(getImageUrl(deck.getHero5())).into(hero5);
            deckModel.setHero5(deck.getHero5());
        }

        deckname.setText(deck.getName());
        desc.setText(deck.getDesc());
        strong.setText(deck.getStrongvs());
        weak.setText(deck.getWeakvs());

        // Checbox de game level
        if (deck.getGame_level_early().equals(1))
            gamelevelEarly.setChecked(true);
        if (deck.getGame_level_mid().equals(1))
            gamelevelMid.setChecked(true);
        if (deck.getGame_level_late().equals(1))
            gamelevelLate.setChecked(true);

        // Checbox de section
        if (deck.getSection_overall().equals(1))
            sectionOverall.setChecked(true);
        if (deck.getSection_pvp().equals(1))
            sectionPVP.setChecked(true);
        if (deck.getSection_pve().equals(1))
            sectionPVE.setChecked(true);
        if (deck.getSection_lab().equals(1))
            sectionLAB.setChecked(true);
        if (deck.getSection_wrizz().equals(1))
            sectionWrizz.setChecked(true);
        if (deck.getSection_soren().equals(1))
            sectionSoren.setChecked(true);

        imageView_profile_picture.setFocusable(true);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageView_hero1:
                holderActive = 1;
                break;
            case R.id.imageView_hero2:
                holderActive = 2;
                break;
            case R.id.imageView_hero3:
                holderActive = 3;
                break;
            case R.id.imageView_hero4:
                holderActive = 4;
                break;
            case R.id.imageView_hero5:
                holderActive = 5;
                break;
        }

        HeroFastListFragment heroFastListFragment = new HeroFastListFragment(heroModels);
        heroFastListFragment.SetOnItemClickListener(new HeroFastListFragment.CerrarTodo() {
            @Override
            public void cierrate(HeroModel heroModel) {
                switch (holderActive){
                    case 1:
                        deckModel.setHero1(heroModel.getName());
                        Picasso.get().load(heroModel.getSmallImage()).into(hero1);
                    break;
                    case 2:
                        deckModel.setHero2(heroModel.getName());
                        Picasso.get().load(heroModel.getSmallImage()).into(hero2);
                    break;
                    case 3:
                        deckModel.setHero3(heroModel.getName());
                        Picasso.get().load(heroModel.getSmallImage()).into(hero3);
                    break;
                    case 4:
                        deckModel.setHero4(heroModel.getName());
                        Picasso.get().load(heroModel.getSmallImage()).into(hero4);
                    break;
                    case 5:
                        deckModel.setHero5(heroModel.getName());
                        Picasso.get().load(heroModel.getSmallImage()).into(hero5);
                    break;
                }

            }
        });
        heroFastListFragment.show(getSupportFragmentManager(), "dpkd");
    }
}


















