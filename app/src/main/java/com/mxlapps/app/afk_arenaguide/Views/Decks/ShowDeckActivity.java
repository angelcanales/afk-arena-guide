package com.mxlapps.app.afk_arenaguide.Views.Decks;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Adapter.DeckCommentsAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.CommentsModel;
import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.Model.VoteModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.AppPreferences;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.DecksViewModel;
import com.mxlapps.app.afk_arenaguide.ViewModel.ExtraViewModel;
import com.mxlapps.app.afk_arenaguide.Views.LoginFacebookActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ShowDeckActivity extends AppCompatActivity {

    private static final String TAG = "afkArenaMainActivity";
    private DeckModel deck;
    private ArrayList<CommentsModel> commentsModel = new ArrayList<>();
    private View rootView;
    View v;
    private DecksViewModel decksViewModel;
    private ExtraViewModel extraViewModel;
    DeckCommentsAdapter adapter;

    ImageView imageViewDeck1;
    ImageView imageViewDeck2;
    ImageView imageViewDeck3;
    ImageView imageViewDeck4;
    ImageView imageViewDeck5;
    TextView textView_deckName1;
    TextView textView_deck_description;
    TextView textView_autor;
    TextView textView_numero_votos;

    TextView textView_weak;
    TextView textView_strong;

    TextView textViewEarly;
    TextView textViewMid;
    TextView textViewLate;

    TextView textView_overall;
    TextView textView_pvp;
    TextView textView_pve;
    TextView textView_lab;
    TextView textView_wrizz;
    TextView textView_soren;
    RatingBar ratingBar;
    Button button_vote;
    ImageView button_send_comment;
    Toolbar toolbar;
    String token;
    CircleImageView imageViewUsuario;

    EditText textInputEditText_nuevo_comentario;
    RecyclerView recycler_comments;



    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_deck);

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        // Toolbar
        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("AFK GUIDE - TEAM DETAIL");
        }


        // Revisa si se esta llegando desde una factura
        Intent intent = getIntent();
        if (intent.hasExtra("deck_data")) {
            String deck = intent.getStringExtra("deck_data");
            Gson gson = new Gson();
            this.deck =  gson.fromJson(deck, DeckModel.class);
        }

        // ViewModels
        decksViewModel = ViewModelProviders.of(ShowDeckActivity.this).get(DecksViewModel.class);
        extraViewModel = ViewModelProviders.of(ShowDeckActivity.this).get(ExtraViewModel.class);

//        Util.initAds(rootView, this, BuildConfig.ADS_DECK_DETAIL);

        initViews();


        requestShowDeck();

        eventoVotar();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
            case R.id.menu_share:

                // Se comparte la pantalla
                Log.d(TAG, "onOptionsItemSelected: ");

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        token = AppPreferences.getInstance(ShowDeckActivity.this).getUserToken();
        if (token.compareToIgnoreCase("") != 0){
            // Se carga la imagen del usuario en los comentarios
            Integer idUsuario = AppPreferences.getInstance(ShowDeckActivity.this).getUsuario_id();
            String[] urlfixed = BuildConfig.API_BASE_URL.split("api/v2");
            String urlImage = urlfixed[0] + "assets/images/users/user_" + idUsuario + ".jpg";

            Picasso.get().load(urlImage).into(imageViewUsuario);
        }

    }

    private void eventoVotar() {
        button_vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                token = AppPreferences.getInstance(ShowDeckActivity.this).getUserToken();

                if (token.compareToIgnoreCase("") != 0){

                    VoteModel voteModel = new VoteModel();
                    voteModel.setUser_id(AppPreferences.getInstance(ShowDeckActivity.this).getUsuario_id());
                    voteModel.setSection("decks");
                    voteModel.setVote(ratingBar.getRating());
                    voteModel.setItem_id(deck.getId());
                    extraViewModel.create_vote(voteModel).observe(ShowDeckActivity.this, new Observer<Resource<DataMaster>>() {
                        @Override
                        public void onChanged(Resource<DataMaster> dataMasterResource) {
                            procesaRespuesta(dataMasterResource, 3);
                        }
                    });

                }else{
                    Intent intent = new Intent(ShowDeckActivity.this, LoginFacebookActivity.class);
                    startActivityForResult(intent, 1001);
                }

            }
        });

    }


    private void cargaInformacionDeck() {
        // Toma toda la informacion de la variable deck que es local

        // Carga Imagenes
        Picasso.get().load(deck.getHero1()).into(imageViewDeck1);
        Picasso.get().load(deck.getHero2()).into(imageViewDeck2);
        Picasso.get().load(deck.getHero3()).into(imageViewDeck3);
        Picasso.get().load(deck.getHero4()).into(imageViewDeck4);
        Picasso.get().load(deck.getHero5()).into(imageViewDeck5);

        textView_deckName1.setText(deck.getName());
        textView_deck_description.setText(deck.getDesc());
        textView_autor.setText("By: " + deck.getAuthor());

        textView_weak.setText(deck.getWeakvs());
        textView_strong.setText(deck.getStrongvs());

        // Se ocultan los booleanos
        textViewEarly.setVisibility(View.GONE);
        textViewMid.setVisibility(View.GONE);
        textViewLate.setVisibility(View.GONE);

        textView_overall.setVisibility(View.GONE);
        textView_pvp.setVisibility(View.GONE);
        textView_pve.setVisibility(View.GONE);
        textView_lab.setVisibility(View.GONE);
        textView_wrizz.setVisibility(View.GONE);
        textView_soren.setVisibility(View.GONE);



            // Game Level
        if (deck.getGame_level_early() == 1){
            textViewEarly.setVisibility(View.VISIBLE);
        }

        if (deck.getGame_level_mid() == 1){
            textViewMid.setVisibility(View.VISIBLE);
        }

        if (deck.getGame_level_late() == 1){
            textViewLate.setVisibility(View.VISIBLE);
        }


        // Section
        if (deck.getSection_overall() == 1){
            textView_overall.setVisibility(View.VISIBLE);
        }

        if (deck.getSection_pvp() == 1){
            textView_pvp.setVisibility(View.VISIBLE);
        }

        if (deck.getSection_pve() == 1){
            textView_pve.setVisibility(View.VISIBLE);
        }

        if (deck.getSection_lab() == 1){
            textView_lab.setVisibility(View.VISIBLE);
        }

        if (deck.getSection_wrizz() == 1){
            textView_wrizz.setVisibility(View.VISIBLE);
        }

        if (deck.getSection_soren() == 1){
            textView_soren.setVisibility(View.VISIBLE);
        }

        ratingBar.setRating(Float.parseFloat(deck.getVotes()));

        if (deck.getNumero_votos() == 0 || deck.getNumero_votos() > 1){
            textView_numero_votos.setText(deck.getNumero_votos() + " votes");
        }else{
            textView_numero_votos.setText(deck.getNumero_votos() + " vote");
        }

        // Se llama al recycler de comentarios
        this.commentsModel  = deck.getComments();
        initRecyclerViewComentarios();

    }

    private void initViews() {
        ratingBar = findViewById(R.id.ratingBar);
        textView_deckName1  = findViewById(R.id.textView_deckName1);
        textView_deck_description  = findViewById(R.id.textView_deck_description);
        textView_autor  = findViewById(R.id.textView_autor);
        imageViewDeck1 = findViewById(R.id.imageViewDeck1);
        imageViewDeck2 = findViewById(R.id.imageViewDeck2);
        imageViewDeck3 = findViewById(R.id.imageViewDeck3);
        imageViewDeck4 = findViewById(R.id.imageViewDeck4);
        imageViewDeck5 = findViewById(R.id.imageViewDeck5);

        textView_weak = findViewById(R.id.textView_desc_weak);
        textView_strong = findViewById(R.id.textView_desc_strong);

        textViewEarly = findViewById(R.id.textViewEarly);
        textViewMid = findViewById(R.id.textViewMid);
        textViewLate = findViewById(R.id.textViewLate);

        textView_overall = findViewById(R.id.textView_overall);
        textView_pvp = findViewById(R.id.textView_pvp);
        textView_pve = findViewById(R.id.textView_pve);
        textView_lab = findViewById(R.id.textView_lab);
        textView_wrizz = findViewById(R.id.textView_wrizz);
        textView_soren = findViewById(R.id.textView_soren);

        ratingBar = findViewById(R.id.ratingBar);
        button_vote = findViewById(R.id.button_vote);
        textView_numero_votos = findViewById(R.id.textView_numero_votos);


        textInputEditText_nuevo_comentario = findViewById(R.id.textInputEditText_nuevo_comentario);
        recycler_comments = findViewById(R.id.recyclerview_comentarios);
        button_send_comment = findViewById(R.id.button_send_comment);

        imageViewUsuario = findViewById(R.id.imageViewUsuario);



        button_send_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // En cuanto se hace click, se esconde el teclado
                hideKeyboard(ShowDeckActivity.this);

                String comentario = (textInputEditText_nuevo_comentario.getText() == null)? "": textInputEditText_nuevo_comentario.getText().toString();
                if (comentario.compareToIgnoreCase("") == 0){
                    Toast.makeText(ShowDeckActivity.this, "Write your comment", Toast.LENGTH_SHORT).show();
                }else{
                    // Valida que este logeado
                    token = AppPreferences.getInstance(ShowDeckActivity.this).getUserToken();
                    if (token.compareToIgnoreCase("") != 0){
                        // Se escribio un comentario, hay que crear el objeto y enviarlo
                        CommentsModel commentsModel = new CommentsModel();
                        commentsModel.setComment(comentario);
                        commentsModel.setItem_id(deck.getId());
                        commentsModel.setUser(String.valueOf(AppPreferences.getInstance(ShowDeckActivity.this).getUsuario_id()));
                        commentsModel.setSection("decks");
                        extraViewModel.createComment(commentsModel).observe(ShowDeckActivity.this, new Observer<Resource<DataMaster>>() {
                            @Override
                            public void onChanged(Resource<DataMaster> dataMasterResource) {
                                procesaRespuesta(dataMasterResource, 2);
                            }
                        });
                    }else{
                        Intent intent = new Intent(ShowDeckActivity.this, LoginFacebookActivity.class);
                        startActivityForResult(intent, 1001);
                    }
                }
            }
        });

        token = AppPreferences.getInstance(ShowDeckActivity.this).getUserToken();
        if (token.compareToIgnoreCase("") != 0){
            // Se carga la imagen del usuario en los comentarios
            Integer idUsuario = AppPreferences.getInstance(ShowDeckActivity.this).getUsuario_id();
            String[] urlfixed = BuildConfig.API_BASE_URL.split("api/v2");
            String urlImage = urlfixed[0] + "assets/images/users/user_" + idUsuario + ".jpg";

            Picasso.get().load(urlImage).into(imageViewUsuario);
        }


    }


    private void requestShowDeck() {
        decksViewModel.show_deck(deck.getId()).observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion) {
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, ShowDeckActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                // Crea copia de listado de heroes
                switch (opcion) {
                    case 1:
                        // Carga informacion del deck con los comentarios
                        this.deck = dataMasterResource.data.getData().getDeck();
                        cargaInformacionDeck();
                        break;
                    case 2:
                        // Respuesta al crear un comentario nuevo

                        if (dataMasterResource.data.getData().isError()){
                            Toast.makeText(this, dataMasterResource.data.getData().getMsg(), Toast.LENGTH_SHORT).show();
                        }else{
                            this.deck = dataMasterResource.data.getData().getDeck();
                            cargaInformacionDeck();
                            textInputEditText_nuevo_comentario.setText("");
                            Toast.makeText(this, "Comments Added!", Toast.LENGTH_SHORT).show();
                        }


                        break;
                    case 3:
                        // Repsusta al enviar la votacion
                        Toast.makeText(this, "Thank you for your vote!", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;
            default:
                break;
        }
    }

    
    private void initRecyclerViewComentarios() {
        if (commentsModel.size() > 0){
            // Cardview is alwasy invisible
            CardView cardview_comentarios = findViewById(R.id.cardview_comentarios);
            cardview_comentarios.setVisibility(View.VISIBLE);
            int numberOfColumns = 1;
            adapter = new DeckCommentsAdapter(commentsModel, ShowDeckActivity.this, 1);
            recycler_comments.setLayoutManager(new GridLayoutManager(ShowDeckActivity.this, numberOfColumns));
            recycler_comments.setNestedScrollingEnabled(false);
            recycler_comments.setHasFixedSize(true);
            recycler_comments.setAdapter(adapter);
        }else{
            Log.d(TAG, "initRecyclerViewComentarios: ----------------------------><");
        }
    }

    @Override
    public void finish() {
        super.finish();
        setResult(RESULT_OK);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
