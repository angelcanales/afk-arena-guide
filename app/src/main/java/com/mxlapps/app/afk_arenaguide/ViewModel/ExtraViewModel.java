package com.mxlapps.app.afk_arenaguide.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mxlapps.app.afk_arenaguide.Model.CommentsModel;
import com.mxlapps.app.afk_arenaguide.Model.VoteModel;
import com.mxlapps.app.afk_arenaguide.Repository.ExtraRepository;
import com.mxlapps.app.afk_arenaguide.Request.Data;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;

public class ExtraViewModel extends AndroidViewModel {

    private ExtraRepository repository;


    public ExtraViewModel(@NonNull Application application) {
        super(application);
        //Se crea una instancia del repositorio
        repository = ExtraRepository.getInstance(application);
    }


    public LiveData<Resource<DataMaster>> getContributors() {

        return repository.getContributors();
    }

    public LiveData<Resource<DataMaster>> getNews() {

        return repository.getNews();
    }

    public LiveData<Resource<DataMaster>> getItems() {

        return repository.getItems();
    }

    public LiveData<Resource<DataMaster>> getRoleDefinitions() {

        return repository.getRoleDefinitions();
    }

    public LiveData<Resource<DataMaster>> getFaq() {

        return repository.getFaq();
    }


    public LiveData<Resource<DataMaster>> getAboutUs() {

        return repository.getAboutUs();
    }

    public LiveData<Resource<DataMaster>> add_suggestion(DataMaster dataMaster) {

        return repository.add_suggestion(dataMaster);
    }

    public LiveData<Resource<DataMaster>> createComment(CommentsModel comment) {

        Data data  = new Data();
        data.setComment(comment);
        DataMaster dataMaster = new DataMaster();
        dataMaster.setData(data);

        return repository.createComment(dataMaster);
    }

    public LiveData<Resource<DataMaster>> create_vote(VoteModel vote) {

        Data data  = new Data();
        data.setVote(vote);
        DataMaster dataMaster = new DataMaster();
        dataMaster.setData(data);

        return repository.create_vote(dataMaster);
    }




}
