package com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Adapter.SkillsAdapter;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.Model.SkillModel;
import com.mxlapps.app.afk_arenaguide.R;

import java.util.ArrayList;

public class SkillsFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String heroString = getArguments().getString("hero");
        Gson gson = new Gson();
        HeroModel hero = gson.fromJson(heroString, HeroModel.class);
        ArrayList<SkillModel> skills = hero.getSkills();

        View v = inflater.inflate(R.layout.fragment_skills, container, false);

        // LLena recycler de skills
        RecyclerView recyclerView = v.findViewById(R.id.recycler_skills_simple);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        SkillsAdapter adapter = new SkillsAdapter(skills);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return v;
    }





}
