package com.mxlapps.app.afk_arenaguide.Views.Decks;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.Data;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.DecksApi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeckSaveNewDeckFragment extends BottomSheetDialogFragment {

    DeckModel deckModel = new DeckModel();
    View v;
    Button button_deck_save;
    TextInputEditText name;
    TextInputEditText desc;
    TextInputEditText strong;
    TextInputEditText weak;
    CheckBox gamelevelEarly;
    CheckBox gamelevelMid;
    CheckBox gamelevelLate;
    CheckBox sectionOverall;
    CheckBox sectionPVP;
    CheckBox sectionPVE;
    CheckBox sectionLAB;
    CheckBox sectionSoren;
    CheckBox sectionWrizz;


    public static DeckSaveNewDeckFragment newInstance() {
        return new DeckSaveNewDeckFragment();
    }

    public interface CerrarTodo{
        public void cierrate();
    }

    public CerrarTodo listener;

    public void SetOnItemClickListener (CerrarTodo mlistener) {
        this.listener = mlistener;
    }


    public DeckSaveNewDeckFragment() {
    }

    public DeckSaveNewDeckFragment(DeckModel deckModel) {
        this.deckModel = deckModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_deck_save_new_deck, container, false);
        Log.d("bottomSheet", "onCreateView: " + deckModel.getHero1());

        initiViews();

        button_deck_save = v.findViewById(R.id.button_deck_save);
        button_deck_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button_deck_save.setEnabled(false);

                // Recolecta los datos
                if (name.getText().toString().compareToIgnoreCase("") == 0|| name.getText().toString().length() > 15){
                    Toast.makeText(getActivity(), "Insert a name for your Deck, max 15 chars", Toast.LENGTH_SHORT).show();
                    button_deck_save.setEnabled(true);
                    return;
                }
                if (desc.getText().toString().compareToIgnoreCase("") == 0 ){
                    Toast.makeText(getActivity(), "Insert a description for your Deck", Toast.LENGTH_SHORT).show();
                    button_deck_save.setEnabled(true);
                    return;
                }else{
                    if (desc.getText().toString().length() < 15){
                        Toast.makeText(getActivity(), "Please increase the description for your Deck", Toast.LENGTH_SHORT).show();
                        button_deck_save.setEnabled(true);
                        return;
                    }
                }

                deckModel.setName(name.getText().toString());
                deckModel.setDesc(desc.getText().toString());

                deckModel.setStrongvs(strong.getText().toString());
                deckModel.setWeakvs(strong.getText().toString());

                deckModel.setGame_level_early((gamelevelEarly.isChecked())? 1:0);
                deckModel.setGame_level_mid((gamelevelMid.isChecked())? 1:0);
                deckModel.setGame_level_late((gamelevelLate.isChecked())? 1:0);

                deckModel.setSection_overall((sectionOverall.isChecked())? 1:0);
                deckModel.setSection_pvp((sectionPVP.isChecked())? 1:0);
                deckModel.setSection_pve((sectionPVE.isChecked())? 1:0);
                deckModel.setSection_lab((sectionLAB.isChecked())? 1:0);
                deckModel.setSection_wrizz((sectionWrizz.isChecked())? 1:0);
                deckModel.setSection_soren((sectionSoren.isChecked())? 1:0);


                Data data  = new Data();
                data.setDeck(deckModel);
                DataMaster dataMaster = new DataMaster();
                dataMaster.setData(data);

                String URL_SERVER_DOCDIGITALES = BuildConfig.API_BASE_URL;
                Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
                HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
                httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .addInterceptor(httpLoggingInterceptor)
                        .build();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(URL_SERVER_DOCDIGITALES)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(okHttpClient)
                        .build();
                DecksApi afk_api = retrofit.create(DecksApi.class);
                Call call = afk_api.createDeck(dataMaster);
                call.enqueue(new Callback<DataMaster>() {
                    @Override
                    public void onResponse(Call<DataMaster> call, Response<DataMaster> response) {
                        if (response.body() != null) {
                            // Todo bien
                            if (response.body().getData() != null){
                                if (!response.body().getData().isError()){
                                    Toast.makeText(getActivity(), "Thank you, your team is now live!", Toast.LENGTH_LONG).show();
                                    listener.cierrate();
                                    dismiss();
                                }else{
                                    Toast.makeText(getActivity(), "Error :(", Toast.LENGTH_LONG).show();
                                }
                            }else{
                                Toast.makeText(getActivity(), "Error :( ..", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Log.d("ERRORRRRRR", "onResponse: " + t.getMessage());
                        button_deck_save.setEnabled(true);
                    }
                });
            }
        });

        return v;
    }


    private void initiViews() {
        name = v.findViewById(R.id.deckName);
        desc = v.findViewById(R.id.deckDesc);
        strong = v.findViewById(R.id.deckStrengt);
        weak = v.findViewById(R.id.deckWeakt);
        gamelevelEarly = v.findViewById(R.id.checkbox_Early);
        gamelevelMid = v.findViewById(R.id.checkbox_Mid);
        gamelevelLate = v.findViewById(R.id.checkbox_Late);
        sectionOverall = v.findViewById(R.id.checkbox_Overall);
        sectionPVP = v.findViewById(R.id.checkbox_PVP);
        sectionPVE = v.findViewById(R.id.checkbox_PVE);
        sectionLAB = v.findViewById(R.id.checkbox_LAB);
        sectionSoren = v.findViewById(R.id.checkbox_Soren);
        sectionWrizz = v.findViewById(R.id.checkbox_Wrizz);
    }

}
