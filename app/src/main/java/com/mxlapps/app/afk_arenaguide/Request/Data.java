
package com.mxlapps.app.afk_arenaguide.Request;

import com.mxlapps.app.afk_arenaguide.Model.AboutUsModel;
import com.mxlapps.app.afk_arenaguide.Model.CommentsModel;
import com.mxlapps.app.afk_arenaguide.Model.ContributorModel;
import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.Model.FaqModel;
import com.mxlapps.app.afk_arenaguide.Model.GuildModel;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.Model.ItemsModel;
import com.mxlapps.app.afk_arenaguide.Model.ListHeroesModel;
import com.mxlapps.app.afk_arenaguide.Model.NewsModel;
import com.mxlapps.app.afk_arenaguide.Model.PinModel;
import com.mxlapps.app.afk_arenaguide.Model.ProfileModel;
import com.mxlapps.app.afk_arenaguide.Model.RolDefinitionModel;
import com.mxlapps.app.afk_arenaguide.Model.StrengthWeaknessModel;
import com.mxlapps.app.afk_arenaguide.Model.SuggestionModel;
import com.mxlapps.app.afk_arenaguide.Model.UserModel;
import com.mxlapps.app.afk_arenaguide.Model.VoteModel;

import java.util.ArrayList;

public class Data {

    private ArrayList<HeroModel> heroes;
    private ArrayList<GuildModel> guilds;
    private ArrayList<ContributorModel> contributors;
    private ArrayList<NewsModel> news;
    private ArrayList<ItemsModel> items;
    private ArrayList<FaqModel> faq;
    private ArrayList<StrengthWeaknessModel> strengths;
    private ArrayList<StrengthWeaknessModel> weaknesses;
    private ArrayList<RolDefinitionModel> rol_definition;
    private HeroModel heroe;
    private StrengthWeaknessModel strengthWeakness;
    private GuildModel guild;
    private AboutUsModel about_us;
    private UserModel user;
    private SuggestionModel suggestion;
    private String msg;
    private boolean error;
    private ArrayList<DeckModel> decks;
    private DeckModel deck;
    private CommentsModel comment;
    private ArrayList<CommentsModel> comments;
    private PinModel pin;
    private VoteModel vote;
    private ArrayList<PinModel> pins;
    private ListHeroesModel list;
    private ProfileModel profile;

    public ProfileModel getProfile() {
        return profile;
    }

    public void setProfile(ProfileModel profile) {
        this.profile = profile;
    }

    public ListHeroesModel getList() {
        return list;
    }

    public void setList(ListHeroesModel list) {
        this.list = list;
    }

    public VoteModel getVote() {
        return vote;
    }

    public void setVote(VoteModel vote) {
        this.vote = vote;
    }

    public PinModel getPin() {
        return pin;
    }

    public void setPin(PinModel pin) {
        this.pin = pin;
    }

    public ArrayList<PinModel> getPins() {
        return pins;
    }

    public void setPins(ArrayList<PinModel> pins) {
        this.pins = pins;
    }

    public ArrayList<CommentsModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentsModel> comments) {
        this.comments = comments;
    }

    public CommentsModel getComment() {
        return comment;
    }

    public void setComment(CommentsModel comment) {
        this.comment = comment;
    }

    public DeckModel getDeck() {
        return deck;
    }

    public void setDeck(DeckModel deck) {
        this.deck = deck;
    }

    public ArrayList<DeckModel> getDecks() {
        return decks;
    }

    public void setDecks(ArrayList<DeckModel> decks) {
        this.decks = decks;
    }

    public SuggestionModel getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(SuggestionModel suggestion) {
        this.suggestion = suggestion;
    }

    public ArrayList<StrengthWeaknessModel> getStrengths() {
        return strengths;
    }

    public void setStrengths(ArrayList<StrengthWeaknessModel> strengths) {
        this.strengths = strengths;
    }

    public ArrayList<StrengthWeaknessModel> getWeaknesses() {
        return weaknesses;
    }

    public void setWeaknesses(ArrayList<StrengthWeaknessModel> weaknesses) {
        this.weaknesses = weaknesses;
    }

    public StrengthWeaknessModel getStrengthWeakness() {
        return strengthWeakness;
    }

    public void setStrengthWeakness(StrengthWeaknessModel strengthWeakness) {
        this.strengthWeakness = strengthWeakness;
    }

    public ArrayList<FaqModel> getFaq() {
        return faq;
    }

    public void setFaq(ArrayList<FaqModel> faq) {
        this.faq = faq;
    }

    public AboutUsModel getAbout_us() {
        return about_us;
    }

    public void setAbout_us(AboutUsModel about_us) {
        this.about_us = about_us;
    }

    public HeroModel getHeroe() {
        return heroe;
    }

    public void setHeroe(HeroModel heroe) {
        this.heroe = heroe;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public ArrayList<ItemsModel> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemsModel> items) {
        this.items = items;
    }

    public ArrayList<RolDefinitionModel> getRol_definition() {
        return rol_definition;
    }

    public void setRol_definition(ArrayList<RolDefinitionModel> rol_definition) {
        this.rol_definition = rol_definition;
    }

    public ArrayList<NewsModel> getNews() {
        return news;
    }

    public void setNews(ArrayList<NewsModel> news) {
        this.news = news;
    }

    public GuildModel getGuild() {
        return guild;
    }

    public void setGuild(GuildModel guild) {
        this.guild = guild;
    }

    public ArrayList<GuildModel> getGuilds() {
        return guilds;
    }

    public void setGuilds(ArrayList<GuildModel> guilds) {
        this.guilds = guilds;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserModel getUserModel() {
        return user;
    }

    public void setUserModel(UserModel user) {
        this.user = user;
    }

    public ArrayList<HeroModel> getHeroes() {
        return heroes;
    }

    public void setHeroes(ArrayList<HeroModel> heroes) {
        this.heroes = heroes;
    }

    public HeroModel getHero() {
        return heroe;
    }

    public void setHero(HeroModel hero) {
        this.heroe = hero;
    }

    public ArrayList<HeroModel> getHeroModels() {
        return heroes;
    }

    public void setHeroModels(ArrayList<HeroModel> heroModels) {
        this.heroes = heroModels;
    }

    public ArrayList<ContributorModel> getContributors() {
        return contributors;
    }

    public void setContributors(ArrayList<ContributorModel> contributors) {
        this.contributors = contributors;
    }
}
