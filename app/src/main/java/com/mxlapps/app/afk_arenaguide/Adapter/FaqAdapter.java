package com.mxlapps.app.afk_arenaguide.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.FaqModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.ItemsViewHolder>{

    private ArrayList<FaqModel> faqmodel;

    public FaqAdapter(ArrayList<FaqModel> faqmodel) {
        this.faqmodel = faqmodel;
    }

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_faq, parent, false);
        return new ItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemsViewHolder holder, int position) {
        final FaqModel item = faqmodel.get(position);
        holder.textView_faq_title.setText(item.getTitle());
        holder.textView_faq_desc.setText(item.getDesc());
//        if (item.getImage() != null && item.getImage().compareToIgnoreCase("") != 0){
//            Picasso.get().load(item.getImage()).into(holder.imageView_faq_image);
//        }else{
//            holder.imageView_faq_image.setVisibility(View.GONE);
//        }
    }

    @Override
    public int getItemCount() {
        return faqmodel.size();
    }

    class ItemsViewHolder extends RecyclerView.ViewHolder {

        TextView textView_faq_desc;
        TextView textView_faq_title;
        ImageView imageView_faq_image;

        ItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_faq_title    = itemView.findViewById(R.id.textView_faq_title);
            textView_faq_desc    = itemView.findViewById(R.id.textView_faq_desc);
            imageView_faq_image    = itemView.findViewById(R.id.imageView_faq_image);

            textView_faq_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (textView_faq_desc.getVisibility() == View.VISIBLE) {
                        textView_faq_desc.setVisibility(View.GONE);
//                        imageView_faq_image.setVisibility(View.GONE);
                    } else {
                        textView_faq_desc.setVisibility(View.VISIBLE);
//                        imageView_faq_image.setVisibility(View.VISIBLE);
                    }

                }
            });
        }
    }
}
