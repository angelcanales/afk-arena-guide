package com.mxlapps.app.afk_arenaguide.Views.Heroes.Fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Model.HeroModel;
import com.mxlapps.app.afk_arenaguide.R;

public class LoreFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String heroString = getArguments().getString("hero");
        Gson gson = new Gson();
        HeroModel hero = gson.fromJson(heroString, HeroModel.class);

        View v = inflater.inflate(R.layout.fragment_lore, container, false);

        TextView textview_lore_text = v.findViewById(R.id.textview_lore_text);
        textview_lore_text.setText(Html.fromHtml(hero.getLore()));

        return v;
    }
}
