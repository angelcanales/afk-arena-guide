package com.mxlapps.app.afk_arenaguide.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


import com.mxlapps.app.afk_arenaguide.Repository.AfkRepository;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;

public class HeroViewModel extends AndroidViewModel {

    private AfkRepository repository;


    public HeroViewModel(@NonNull Application application) {
        super(application);
        //Se crea una instancia del repositorio
        repository = AfkRepository.getInstance(application);
    }


    public LiveData<Resource<DataMaster>> getHeroList(String gameLevel,  String section, String rarity,  String classe,  String race_name) {

        return repository.getHeroList(gameLevel, section,rarity, classe, race_name);
    }

    public LiveData<Resource<DataMaster>> list_advanced(DataMaster dataMaster) {

        return repository.list_advanced(dataMaster);
    }


    public LiveData<Resource<DataMaster>> getHeroDetail(Integer hero_id) {

        return repository.getHeroDetail(hero_id);
    }




}
