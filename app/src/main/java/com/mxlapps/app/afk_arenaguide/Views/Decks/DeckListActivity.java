package com.mxlapps.app.afk_arenaguide.Views.Decks;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.mxlapps.app.afk_arenaguide.Adapter.DecksAdapter;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.mxlapps.app.afk_arenaguide.Request.DataMaster;
import com.mxlapps.app.afk_arenaguide.Service.Resource;
import com.mxlapps.app.afk_arenaguide.Utils.Util;
import com.mxlapps.app.afk_arenaguide.ViewModel.DecksViewModel;
import com.watermark.androidwm.WatermarkBuilder;
import com.watermark.androidwm.bean.WatermarkText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

import static android.view.Gravity.RIGHT;


public class DeckListActivity extends AppCompatActivity {

    private ArrayList<DeckModel> deckModels;
    private static final String TAG = "afkArenaMainActivity";
    private View rootView;
    View v;
    private DecksViewModel decksViewModel;
    SwipeRefreshLayout swipeRefreshLayout;
    private NavigationView navigationView;
    private String ORDER_BY = "desc";
    private String COLUMN = "votes";
    private DrawerLayout drawer;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_list);

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        // ViewModels
        decksViewModel = ViewModelProviders.of(DeckListActivity.this).get(DecksViewModel.class);

        drawer = findViewById(R.id.coordinatorLayout_listado_decks);


        requestCargarListaDecks();

        FloatingActionButton button_drawer_filtros = findViewById(R.id.button_crear_deck);
        button_drawer_filtros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeckListActivity.this, CreateDeckActivity.class);
                startActivityForResult(intent, 2001);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });


        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_deckLIst);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("AFK GUIDE - TEAMS");
            getSupportActionBar().setSubtitle("List of teams");
        }


//        Util.initAds(rootView, this, BuildConfig.ADS_DECK_LIST);

        swipeRefreshLayout =  findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestCargarListaDecks();
            }
        });


        navigationView = findViewById(R.id.nav_view_filter);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return false;
            }
        });
        final View parentView = navigationView.getHeaderView(0);

        RadioGroup group_asc_desc = parentView.findViewById(R.id.group_asc_desc);
        group_asc_desc.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                switch (checkedId){
                    case R.id.order_asc:
                        ORDER_BY = "ASC";
                        break;
                    case R.id.order_desc:
                        ORDER_BY = "DESC";
                        break;
                }
                requestCargarListaDecks();
            }
        });
        RadioGroup orderby_group = parentView.findViewById(R.id.orderby_group);
        orderby_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                switch (checkedId){
                    case R.id.orderby_votes:
                        COLUMN = "votes";
                        break;
                    case R.id.orderby_name:
                        COLUMN = "name";
                        break;
                }
                requestCargarListaDecks();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;

            case R.id.menu_filtro:
                Log.d(TAG, "onOptionsItemSelected: ji");
                drawer.openDrawer(RIGHT);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_deck_list_filter_toolbar, menu);
        return true;
    }



    private void initRecyclerView(final ArrayList<DeckModel> deckModelsInternal) {
        final RecyclerView recyclerView = findViewById(R.id.recyclerview_deck_list);

        int numberOfColumns = 1;
        DecksAdapter adapter = new DecksAdapter(deckModelsInternal, DeckListActivity.this, 1);
        recyclerView.setLayoutManager(new GridLayoutManager(DeckListActivity.this, numberOfColumns));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        adapter.SetOnItemClickListener(new DecksAdapter.OnItemClickListener() {
            @Override
            public void onDeckCardClick(int position) {
                Log.d(TAG, "onDeckCardClick: click en deck");

                DeckModel deckModel = new DeckModel();
                deckModel = deckModelsInternal.get(position);
                Gson gson = new Gson();
                String deck = gson.toJson(deckModel);
                Intent intent = new Intent(DeckListActivity.this, ShowDeckActivity.class);
                intent.putExtra("deck_data", deck);
                startActivityForResult(intent, 2001);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

            }
        });

        adapter.OnItemFacebookShare(new DecksAdapter.OnItemFacebookShare() {
            @Override
            public void onShareClick(int position) {
                DeckModel deckModel = deckModelsInternal.get(position);
                Log.d(TAG, "onShareClick: " + deckModel.getHero1());


                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(DeckListActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Permission is not granted
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(DeckListActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        // No explanation needed; request the permission
                        ActivityCompat.requestPermissions(DeckListActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                1);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    // Permission has already been granted
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("image/*");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    i.putExtra(Intent.EXTRA_STREAM, getImageUri(getApplicationContext(), getScreenshotFromRecyclerView(recyclerView, position)));
                    try {
                        startActivity(Intent.createChooser(i, "My Profile ..."));
                    } catch (android.content.ActivityNotFoundException ex) {

                        ex.printStackTrace();
                    }

                }





            }
        });

        swipeRefreshLayout.setRefreshing(false);

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private Bitmap getScreenshotFromRecyclerView(RecyclerView view, int position) {
        RecyclerView.Adapter adapter = view.getAdapter();
        Bitmap bigBitmap = null;
        if (adapter != null) {
            int size = adapter.getItemCount();
            int height = 0;
            Paint paint = new Paint();
            int iHeight = 0;
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

            // Use 1/8th of the available memory for this memory cache.
            final int cacheSize = maxMemory / 8;
            LruCache<String, Bitmap> bitmaCache = new LruCache<>(cacheSize);
            for (int i = 0; i < size; i++) {
                if (i == position) {

                    RecyclerView.ViewHolder holder = adapter.createViewHolder(view, adapter.getItemViewType(i));
                    adapter.onBindViewHolder(holder, i);
                    holder.itemView.measure(View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY),
                            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                    holder.itemView.layout(0, 0, holder.itemView.getMeasuredWidth(), holder.itemView.getMeasuredHeight());
                    holder.itemView.setDrawingCacheEnabled(true);
                    holder.itemView.buildDrawingCache();
                    Bitmap drawingCache = holder.itemView.getDrawingCache();
                    if (drawingCache != null) {

                        bitmaCache.put(String.valueOf(i), drawingCache);
                    }

                    height += holder.itemView.getMeasuredHeight();
                }
            }

            bigBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), height, Bitmap.Config.ARGB_8888);
            Canvas bigCanvas = new Canvas(bigBitmap);
            bigCanvas.drawColor(Color.WHITE);

            for (int i = 0; i < size; i++) {
                if (i == position){
                    Bitmap bitmap = bitmaCache.get(String.valueOf(i));
                    bigCanvas.drawBitmap(bitmap, 0f, iHeight, paint);
                    iHeight += bitmap.getHeight();
                    bitmap.recycle();
                }
            }

        }


        WatermarkText watermarkText = new WatermarkText("AFK ARENA GUIDE")
                .setPositionX(0.5)
                .setPositionY(0.0)
                .setTextAlpha(180)
                .setTextSize(18)
                .setTextColor(Color.BLACK)
                .setTextFont(R.font.amaranth_bold)
                .setTextShadow(0.0f, 1, 1, Color.DKGRAY);

        Bitmap bitmap = WatermarkBuilder
                .create(this, bigBitmap)
                .loadWatermarkText(watermarkText)
                .setTileMode(false) // select different drawing mode.
                .getWatermark()
                .getOutputImage();

        return bitmap;
    }




        private void requestCargarListaDecks() {
        decksViewModel.getDecks(ORDER_BY, COLUMN).observe(this, new Observer<Resource<DataMaster>>() {
            @Override
            public void onChanged(Resource<DataMaster> dataMasterResource) {
                procesaRespuesta(dataMasterResource, 1);
            }
        });
    }

    private void procesaRespuesta(Resource<DataMaster> dataMasterResource, int opcion) {
        /* Este opcion va a poder procesar la respuesta del server y ejecutar el callback dependiendo del parametro @opcion*/
        switch (dataMasterResource.status) {
            case ERROR:
                Util.stopLoading(rootView);
                Util.alertaMensajeCtx(dataMasterResource.message, DeckListActivity.this);
                break;
            case LOADING:
                Util.startLoading(rootView);
                break;
            case SUCCESS:
                Util.stopLoading(rootView);
                // Crea copia de listado de heroes
                switch (opcion) {
                    case 1:
                        deckModels = dataMasterResource.data.getData().getDecks();
                        initRecyclerView(deckModels);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        requestCargarListaDecks();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
