package com.mxlapps.app.afk_arenaguide.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mxlapps.app.afk_arenaguide.Model.DeckModel;
import com.mxlapps.app.afk_arenaguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DecksAdapter extends RecyclerView.Adapter<DecksAdapter.HeroViewHolder> {

    private ArrayList<DeckModel> deckModels;
    private OnItemClickListener mlistener;
    private OnItemFacebookShare mshareListener;
    Context ctx;
    int modo = 1;


    public void OnItemFacebookShare (OnItemFacebookShare mshareListener) {
        this.mshareListener = mshareListener;
    }

    public interface OnItemFacebookShare {
        void onShareClick(int position);
    }


    public void SetOnItemClickListener (OnItemClickListener mlistener) {
        this.mlistener = mlistener;
    }

    public interface OnItemClickListener {
        void onDeckCardClick(int position);
    }





    public DecksAdapter(ArrayList<DeckModel> deckModels, Context context, int modo) {
        this.deckModels = deckModels;
        this.ctx = context;
        this.modo = modo;
    }

    @NonNull
    @Override
    public DecksAdapter.HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_deck, parent, false);
        return new HeroViewHolder(view, mlistener, mshareListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final DecksAdapter.HeroViewHolder holder, int position) {
        DeckModel deck = deckModels.get(position);
        holder.name.setText(deck.getName());
        holder.votosPromedio.setText(String.valueOf(deck.getVotes()));
        if (deck.getNumero_votos() == 0 || deck.getNumero_votos() > 1){
            holder.cantidadVotos.setText(deck.getNumero_votos() + " votes");
        }else{
            holder.cantidadVotos.setText(deck.getNumero_votos() + " vote");
        }


        Picasso.get().load(deck.getFb_image()).into(holder.imageView_userdeckpic);
        Picasso.get().load(deck.getHero1()).into(holder.pin1);
        Picasso.get().load(deck.getHero2()).into(holder.pin2);
        Picasso.get().load(deck.getHero3()).into(holder.pin3);
        Picasso.get().load(deck.getHero4()).into(holder.pin4);
        Picasso.get().load(deck.getHero5()).into(holder.pin5);
    }


    @Override
    public int getItemCount() {
        return deckModels.size();
    }

    public class HeroViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView cantidadVotos;
        TextView votosPromedio;
        ImageView facebookShare;
        CircleImageView pin1;
        CircleImageView pin2;
        CircleImageView pin3;
        CircleImageView pin4;
        CircleImageView pin5;
        ImageView imageView_userdeckpic;
        CardView cardView_pin_item;

        public HeroViewHolder(@NonNull View itemView, final OnItemClickListener listener, final OnItemFacebookShare mshareListener) {
            super(itemView);

            imageView_userdeckpic = itemView.findViewById(R.id.imageView_userdeckpic);
            facebookShare = itemView.findViewById(R.id.imageView_fb_share);
            pin1 = itemView.findViewById(R.id.imageView_pin1);
            pin2 = itemView.findViewById(R.id.imageView_pin2);
            pin3 = itemView.findViewById(R.id.imageView_pin3);
            pin4 = itemView.findViewById(R.id.imageView_pin4);
            pin5 = itemView.findViewById(R.id.imageView_pin5);
            name = itemView.findViewById(R.id.textView_coment);
            cantidadVotos = itemView.findViewById(R.id.textView_votos);
            votosPromedio = itemView.findViewById(R.id.textView_cost);
            cardView_pin_item = itemView.findViewById(R.id.cardView_section_item);

            cardView_pin_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        listener.onDeckCardClick(getAdapterPosition());
                    }
                }
            });

            facebookShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mshareListener != null){
                        mshareListener.onShareClick(getAdapterPosition());
                    }
                }
            });


        }
    }

}
