package com.mxlapps.app.afk_arenaguide.Views.Sections;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.mxlapps.app.afk_arenaguide.BuildConfig;
import com.mxlapps.app.afk_arenaguide.Model.NewsModel;
import com.mxlapps.app.afk_arenaguide.R;

public class NewsInformationBottomSheetFragment extends BottomSheetDialogFragment {

    NewsModel newsModel;
    View v;
    TextView noticia;
    WebView webView;


    public static NewsInformationBottomSheetFragment newInstance() {
        return new NewsInformationBottomSheetFragment();
    }


    public NewsInformationBottomSheetFragment() {
    }

    public NewsInformationBottomSheetFragment(NewsModel newsModel) {
        this.newsModel = newsModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.bottom_sheet_news_information, container, false);

        webView = v.findViewById(R.id.webview);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.loadUrl(BuildConfig.API_BASE_URL + "extras/new_show/" + newsModel.getId());

        return v;
    }



}
